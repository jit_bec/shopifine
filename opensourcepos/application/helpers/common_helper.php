<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


    
    
    function check_if_exists_in_array($array, $key, $value)
    {
        $results = array();

        if (is_array($array))
        {
            if (isset($array[$key]) && $array[$key] == $value)
                $results[] = $array;

            foreach ($array as $subarray)
                $results = array_merge($results, check_if_exists_in_array($subarray, $key, $value));
        }

        return $results;
    }
    
    

?>
