<html>
    <head>
       <?php $this->load->view("common/header"); ?>
        <script type="text/javascript" src="http://static.jstree.com/v.1.0pre/jquery.jstree.js"></script>
        <style>
            .ui-combobox > input{
                width:20em;
            }
            
            .ui-combobox > a{
                left:170px;
            }
            .column {width:400px;}
            .field {width:45em;}
        </style>
         <script>
            $(document).ready(function(){
                
                
                 $.validator.addMethod('treerequired', function(value, element) {
                    var selArray = $('#treeViewDiv').jstree('get_selected');
                    console.log(selArray.length);
                    alert (selArray.length);
                    if (selArray.length == 0){
                       
                            return false;
                       
                    }
                    return true;
                }, 'Please select a category');

                $( "#productForm" ).validate({
                    errorPlacement: function(error, element) {
                        error.appendTo( element.parent());
                    }//,
                    //            rules: {
                    //                        mfrOp: {
                    //                            comboBoxrequired: true
                    //                        }
                    //                    }
            
                }
            );   
                
                
                 $( "#dialog:ui-dialog" ).dialog( "destroy" );
                 
                  $("#productOp").combobox();  
                  
                  
                 
                $( "#dialog-form" ).dialog({
                    autoOpen: false,
                    height: 'auto',
                    width: '40%',
                    position:[350,25],
                    modal: true,
                    buttons: {
                        "Create the Product": function() {
                           var bValid =  $("#productForm").valid();
                    
                            if (bValid ){
                                var selectedIds = [];
                                $('#treeViewDiv').jstree('get_selected').each(function(){
                                    selectedIds.push($(this).attr('id'));
                                });
                        
                                $.ajax({
                                    url:"index.php/products/addcategoryToProduct",
                                    data: {
                                            
                                            productOp : $("#productOp").val(),
                                            productIp: $("#productOp-input").val(),
                                            
//                                            categoryOp: $("#categoryOp").val(),
//                                            categoryIp: $("#categoryOp-input").val(),
                                                category:JSON.stringify(selectedIds)
                                            },
                                
                                    type:"POST",
                                    success:function(serverresponse)
                                    {
//                                        var response = JSON.parse(serverresponse);
//                                        emptyMessages();
//                                        if (response.status=='error'){
//                                          
//                                            showErrorMessage(response.message)
//                                        }
//                                        else  {
                                            showSuccessMessage("Product Suceessfully Added To category");
                                            $("#resources").trigger("reloadGrid");
                                        //}
                                    }
                                });
                        
                                $( this ).dialog( "close" );
                            }
                    
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        }
                    },
                    close: function() {
                        //allFields.val( "" ).removeClass( "ui-state-error" );
                        $("#productForm").data('validator').resetForm();
                        $('#productForm')[0].reset();
                    }
                });
                
                
                //user grid
                
                
                
                var myGrid = $("#resources"),lastsel2;
                var editparameters = {
                    "keys" : true,
                    "oneditfunc" : null,
                    "successfunc" : function(){
                        var successStatus = "Resources successfully edited";
                        showSuccessMessage(successStatus);
                        lastsel2=undefined;
                        myGrid.trigger("reloadGrid");
                        return true;
                    },
                    "aftersavefunc" : null,
                    "errorfunc": function(response)
                        {
                            var errorStatus = " Resources could not be updated due to internal error";
                            showErrorMessage(errorStatus);
                            lastsel2=undefined;
                        },
                    "afterrestorefunc" : null,
                    "restoreAfterError" : true,
                    "mtype" : "POST"
                };
                myGrid.jqGrid({
                    url:'index.php/products/populateProductCategoryMapping',
                    datatype: 'json',
                    mtype: 'GET',
                    colNames:['Product','Barcode','Product ID','Category ID','Category'],
                    colModel :[ 
                        {name:'product_name', index:'product_name', width:140, align:'right',editable:false,search:false},
                        {name:'barcode', index:'barcode', width:140, align:'right',editable:false},
                        {name:'product_id', index:'product_id', width:100, align:'right',hidden:true},
                        {name:'category_id', index:'category_id', width:100, align:'right',hidden:true},
                        {name:'category_name', index:'category_name', width:140, align:'right',editable:false},
                        
                    ],
                    pager: '#pager',
                    rowNum:10,
                    rowList:[5,10,20],
                    sortname: 'id',
                    sortorder: 'desc',
                    viewrecords: true,
                    gridview: true,
                    ignoreCase:true,
                    rownumbers:true,
                    height:'auto',
                    width:'auto',
                    multiselect:true,
                    caption: 'Product Category Mapping',
            
                    jsonReader : {
                        root:"productcategorydata",
                        page: "page",
                        total: "total",
                        records: "records",
                        cell: "dprow",
                        id: "id"
                    },
                    onSelectRow: function(id){if(id && id!==lastsel2){
                            myGrid.restoreRow(lastsel2);
                            myGrid.editRow(id,editparameters);
                            lastsel2=id;
                        }
                    },
                    editurl:'index.php/products/deleteProductCategoryMapping'
                    
                }).navGrid("#pager",{edit:false,add:false,del:true,search:false},
               /* edit Option*/ {},
            /* Add Option*/     {reloadAfterSubmit:true,recreateForm:true},
                                                 {},{},{});
               
                myGrid.jqGrid('filterToolbar', {stringResult: true, searchOnEnter: true, defaultSearch : "cn"});
                myGrid.jqGrid('navButtonAdd','#pager',{
                    caption:"", 
                    title:"Add Category To Product",
                    buttonicon:"ui-icon-plus",
                    id:"add_resources",
                    onClickButton : function () { 
                         $('#treeViewDiv').jstree('deselect_all');
                                 $( "#dialog-form" ).dialog( "open" );
                                 } 
                 });
                 $("#add_resources").insertBefore("#del_resources")
                 
                  $("#treeViewDiv")
                        .jstree({
               "plugins" : ["themes", "json_data", "ui"],
               "json_data" : {
                   "ajax" : {
                       "type": 'GET',
                       "url": function (node) {
                           var nodeId = "";
                           var url = ""
                           if (node == -1)
                           {
                               url = "index.php/utilities/renderParents";
                           }
                           else
                           {
                               nodeId = node.attr('id');
                               url = "index.php/utilities/renderChildren";
                           }

                           return url;
                       },
                       data : function(node) {
                           if (node != -1){
                               return {

                                 "nodeid":$.trim(node.attr('id'))
                               }
                           }
                   },
                       "success": function (new_data) {
                           return new_data;
                       }
                   }
               }});  
//               $("#test").click(function (e){
//               e.preventDefault();
//               $("#resources").searchGrid();
//               
//               })
            });
        </script>
    </head>
     
    <body>
         <?php $this->load->view("common/menubar"); ?>
        
        <div id ="dialog-form">
            <h1 id="formHeader">Add New Product Entity</h1>   
            <form id="productForm">
                <fieldset>
                    
                    <div class="row">
                        <div class="column">
                            <div class="field">
                                <label for="productOp">Products:</label>  
                                <select name="productOp" id ="productOp" class="required"> 
                                    <option value="">Choose 
                                        <?= $productOptions ?> 
                                </select>
                                <!--<button id="test"> test</button>-->
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="column">
                            <div class="field">
                                <label for="categoryOp">Category:</label>  
                                <div id="treeViewDiv" style="display:inline-block">
                                </div>
                                <input id="treeViewHidden" name ="treeViewHidden" style="display:inline-block" class="treerequired" type="hidden"/>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        
        <div style="display: block;height: 100%;left:0em;" class="shopifine-ui-dialog ui-dialog-extra-small ui-widget ui-widget-content ui-corner-all" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog-form">
            <?php $this->load->view("common/message"); ?>
            <div class="table-grid">
                <h1 id="table header">Products And Categories</h1>
                <table id="resources"><tr><td/></tr></table> 
                <div id="pager"></div>
            </div>
        </div>
        <?php $this->load->view("partial/footer"); ?>
    </body>
</html>
