<html>
    <head>
       <?php $this->load->view("common/header"); ?>
<!--        <script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>-->
        <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/shopifine.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>css/ui.jqgrid.css" />
        <script src="<?php echo base_url(); ?>js/i18n/grid.locale-en.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/jquery.jqGrid.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
<!--        <script src="http://crypto-js.googlecode.com/svn/tags/3.0.2/build/rollups/md5.js"></script>-->
         <script>
            $(document).ready(function(){
                
                
                //user grid
                
                var myGrid = $("#resources"),lastsel2;
                var editparameters = {
                    "keys" : true,
                    "oneditfunc" : null,
                    "successfunc" : function(){
                        var successStatus = "Resources successfully edited";
                        showSuccessMessage(successStatus);
                        lastsel2=undefined;
                        myGrid.trigger("reloadGrid");
                        return true;
                    },
                    "aftersavefunc" : null,
                    "errorfunc": function(response)
                        {
                            var errorStatus = " Resources could not be updated due to internal error";
                            showErrorMessage(errorStatus);
                            lastsel2=undefined;
                        },
                    "afterrestorefunc" : null,
                    "restoreAfterError" : true,
                    "mtype" : "POST"
                };
                myGrid.jqGrid({
                    url:'index.php/acls/populateResources',
                    datatype: 'json',
                    mtype: 'GET',
                    colNames:['Resource','Resource Type','Parent Resource','Description','UI Display Name','Relative Path','Relative Order'],
                    colModel :[ 
                        {name:'resource', index:'resource', width:100, align:'right',editable:true,editrules:{required:true},editoptions:{size:"20",maxlength:"30"}},
                        {name:'resource_type_id', index:'resource_type_id', width:80, align:'right',editable:true,editrules:{required:true},edittype:"select",editoptions:{dataUrl:"index.php/acls/populateResourceTypesEdit",buildSelect:function(response)
                        {
                            var select = "<select name=" + "resOpEdit" + "id =" +"resOpEdit" +">" +
                                        "<option value=" + ">Select one..." + response + "</select>";
                                    
                            return select;
                        }}},
                        {name:'parent_id', index:'parent_id', width:100, align:'right',editable:true,edittype:"select",editoptions:{dataUrl:"index.php/acls/populateParentResourcesEdit",buildSelect:function(response)
                        {
                            var select = "<select name=" + "parentOpEdit" + "id =" +"parentOpEdit" +">" +
                                        "<option value=" + ">Select one..." + response + "</select>";
                                    
                            return select;
                        }}},
                        {name:'description',index:'description', width:120, align:'right',editable:true,editoptions:{size:"20",maxlength:"30"}},
                        {name:'ui_display_name', index:'ui_display_name', width:100, align:'right',editable:true,editrules:{},editoptions:{size:"20",maxlength:"30"}},
                        {name:'relative_path_link', index:'relative_path_link', width:140, align:'right',editrules:{required:true},editable:true,editoptions:{size:"50",maxlength:"60"}},
                        {name:'relative_order_in_category', index:'relative_order_in_category', width:80, align:'right',editable:true,editoptions:{size:"20",maxlength:"30"}}
                    ],
                    pager: '#pager',
                    rowNum:10,
                    rowList:[5,10,20],
                    sortname: 'id',
                    sortorder: 'desc',
                    viewrecords: true,
                    gridview: true,
                    ignoreCase:true,
                    rownumbers:true,
                    height:'auto',
                    width:'auto',
                    caption: 'Resources',
            
                    jsonReader : {
                        root:"resourcedata",
                        page: "page",
                        total: "total",
                        records: "records",
                        cell: "dprow",
                        id: "id"
                    },
                    onSelectRow: function(id){if(id && id!==lastsel2){
                            myGrid.restoreRow(lastsel2);
                            myGrid.editRow(id,editparameters);
                            lastsel2=id;
                        }
                    },
                    editurl:'index.php/acls/editResource'
                    
                }).navGrid("#pager",{edit:false,add:true,del:false,search:false},
               /* edit Option*/ {height:280,reloadAfterSubmit:false,closeAfterEdit:true,recreateForm:true,checkOnSubmit:true},
            /* Add Option*/     {reloadAfterSubmit:true,recreateForm:true,beforeShowForm:function(form){
                                var roleElem = $("#tr_role_name",form);
                                $('<tr class="FormData" id="tr_username">\n\
                                    <td class="CaptionTD">User Name</td>\n\
                                    <td class="DataTD">&nbsp;<input type="text" size="20" maxlength="30" id="username" \n\
                                    name="username" role="textbox" class="FormElement ui-widget-content ui-corner-all">\n\
                                    </td></tr>').insertBefore(roleElem);
                                $('<tr class="FormData" id="tr_password">\n\
                                    <td class="CaptionTD">Temporary Password</td>\n\
                                    <td class="DataTD">&nbsp;<input type="text" size="20" maxlength="30" id="password" \n\
                                    name="password" role="textbox" class="FormElement ui-widget-content ui-corner-all">\n\
                                    </td></tr>').insertBefore(roleElem);
                                                   
                                }                  
                },{},{},{});
               
                myGrid.jqGrid('filterToolbar', {stringResult: true, searchOnEnter: true, defaultSearch : "cn"});
                
                    
            });
        </script>
    </head>
     
    <body>
         <?php $this->load->view("common/menubar"); ?>
        
        <div style="display: block;height: 100%;width:90%;left:0em;" class="shopifine-ui-dialog ui-widget ui-widget-content ui-corner-all" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog-form">
            <?php //$this->load->view("common/message"); ?>
            <div class="table-grid">
                <h1 id="table header">Resources</h1>
                <table id="resources"><tr><td/></tr></table> 
                <div id="pager"></div>
            </div>
        </div>
        <?php $this->load->view("partial/footer"); ?>
    </body>
</html>
