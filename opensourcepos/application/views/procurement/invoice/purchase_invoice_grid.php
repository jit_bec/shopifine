<html>
    <head>
       <?php $this->load->view("common/header"); ?>
<!--        <script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>-->
        <style>
            .column {
            float: left;
            padding: 1em;
            width:30%;
            }
            
            .extra-wide{
                width:95%;
            }
            .field{
                width:100%;
            }
            .ui-widget-header {height:12px;}
            .quote-column {
            float: left;
            padding-bottom: 0.5em;
            width: 95%;
            }
            .ui-combobox {
                width:14em;
            }
            .ui-combobox-input{
                width:12em;
            }
             #supplierOp-input{
                width:10em;
            }
            #warehouseOp-input{
                width:10em;
            }
             
            .calculated {
                color: green;
                font-size: 90%;
            }
            .row{
                width:95%;
            }
            
            .shopifine-ro-label {
                float: left;
                padding-right: 0.5em;
                width: 50%;
                word-wrap: break-word;
                color:#2E6E9E;
            }

            .shopifine-output {
             float: right;
             width: 45%;
             word-wrap: break-word;
             font-weight:bold;
            }
            
            .ui-tabs {
                height: 80%;
                margin: 0 auto;
                width: 70%;
                left:0;
            }
            #notetab {
                height:30em;
            }
            #details {
                height:12em;
            }
            .ui-tabs-nav{
                height:22px;
            }
            .labeldiv {
                color: #2E6E9E;
                float: left;
                font-size: 110%;
                font-weight: bold;
                margin-right: .5em;
                width: 35%;
                word-wrap: break-word;
            }
            .valuediv {
                float: left;
                font-weight: bold;
                width: 45%;
                word-wrap: break-word;
            }
            label.error {
                margin-right: .5em;
            }
            #status-message-li{
                color: red;
                font-size: 110%;
                font-style: italic;
                margin: 0 auto;
                width: 80%;
            }
            
            .table-grid {
                padding-top: 2em;
            }
            
         
            .help-message {
                color: green;
                float: right;
                font-size: 90%;
                font-style: italic;
                width: 65%;
            }
        </style>
       
        <script type="text/javascript">
                $(function() {
         $.validator.addMethod('tallyamount', function (value, el) {
            var already_paid = parseFloat($("#amount_paid_form").text());
            var pay_amount = parseFloat($("#amount").val());
            var total_invoiced_value = parseFloat($("#total_value_form").text());
            var total_paid= already_paid + pay_amount;
            if (total_paid>total_invoiced_value){
                return false;
            }
            
            else {
                return true;
            }
                },"Total Payment Exceeds Invoiced ");
         $("#paymentForm").validate({rules:{
                 
                 amount:{                  
                     required:true,
                     number:true,
                     minStrict:0,
                     tallyamount:true
                 }
                 
         }}
     );
        // Main Request For Quotation Grid    
        function invFormatter ( cellvalue, options, rowObject )
        {
        // format the cellvalue to new format
        return 'PINV-' + cellvalue;
        }
        function poFormatter ( cellvalue, options, rowObject )
        {
        // format the cellvalue to new format
        return 'PO-' + cellvalue;
        }
        
        var myGrid = $("#orders");
                
                
                myGrid.jqGrid({
                    url:'index.php/procurement/populateInvoicesToPay',
                    datatype: 'json',
                    mtype: 'GET',
                    colNames:['Invoice Reference','Total Value','Amount Paid','Status','Invoiced By','Order Id','Order Ref'],
                    colModel :[ 
                        {name:'reference', index:'reference', width:120, align:'right',editable:false,formatter:invFormatter},
                        {name:'total_value', index:'total_value',editable:false,align:'right',width:100},
                        {name:'amount_paid', index:'amount_paid',editable:false,align:'right',width:120},
                        {name:'status', index:'status',editable:false,align:'right'},
                        {name:'invoiced_by', index:'invoiced_by', width:100, align:'right',editable:false},
                        {name:'order_id', index:'order_id',hidden:true},
                        {name:'order_reference', index:'order_reference',editable:false, width:100, align:'right',formatter:poFormatter}
                        
                    ],
                    pager: '#pager',
                    rowNum:10,
                    rowList:[5,10,20],
                    sortname: 'id',
                    sortorder: 'desc',
                    viewrecords: true,
                    gridview: true,
                    
                    
                    ignoreCase:true,
                    rownumbers:true,
                    height:'auto',
                    width:680,
                    caption: 'Invoices',
                    onSelectRow: function (){
                        $( "#dialog-form" ).dialog("open");
                    },
            
                    jsonReader : {
                        root:"quotedata",
                        page: "page",
                        total: "total",
                        records: "records",
                        cell: "dprow",
                        id: "id"
                    },
                    
                    subGrid:true,
                    subGridRowExpanded: function(subgrid_id, row_id) {
                            // we pass two parameters
                            // subgrid_id is a id of the div tag created whitin a table data
                            // the id of this elemenet is a combination of the "sg_" + id of the row
                            // the row_id is the id of the row
                            // If we wan to pass additinal parameters to the url we can use
                            // a method getRowData(row_id) - which returns associative array in type name-value
                            // here we can easy construct the flowing
                            var subgrid_table_id, pager_id;
                            subgrid_table_id = subgrid_id+"_t";
                            
                            pager_id = "p_"+subgrid_table_id;
                            
                            $("#"+subgrid_id).html("<table id='"+subgrid_table_id+"' class='scroll'></table><div id='"+pager_id+"' class='scroll'></div>");
                            jQuery("#"+subgrid_table_id).jqGrid({
                                    url:'index.php/procurement/populateInvoiceItems?invoiceId='+row_id,
                                    datatype: 'json',
                                    colNames:['Product Id','Product','Invoiced Quantity','Invoiced Value'/*,'Owner','Status','Order Id','Order Ref','Quote Ref','Owner','Needed By Date'*/],
                                        colModel :[ 
//                                            {name:'reference', index:'reference', width:80, align:'right',editable:false},
                                            {name:'product_id', index:'product_id',editable:false,align:'right',hidden:true},
                                            {name:'name', index:'name', width:140, align:'right',editable:false},
                    //                        {name:'estimated_value', index:'estimated_value', width:100, align:'right',editable:false,editoptions:{size:"20",maxlength:"30"}},
                    //                        {name:'owner', index:'owner', width:140, align:'right',editable:true,editoptions:{size:"20",maxlength:"30"}},
                                            {name:'invoiced_quantiy', index:'invoiced_quantiy', width:100, align:'right',editable:false,editoptions:{size:"20",maxlength:"30"}},
                                            {name:'total_invoiced_value', index:'total_invoiced_value',editable:false,align:'right'}
                                            
                                        ],
                                    rowNum:20,
                                    pager: pager_id,
                                    sortname: 'id',
                                    sortorder: "asc",
                                    height: '100%',
                                   
                                    
                                    jsonReader : {
                                        root:"invoiceitemdata",
                                        page: "page",
                                        total: "total",
                                        records: "records",
                                        cell: "dprow",
                                        id: "id"
                                    }
                                });
                            jQuery("#"+subgrid_table_id).jqGrid('navGrid',"#"+pager_id,{edit:false,add:false,del:false,search:false,view:true});
                             
                    }
                }).navGrid("#pager",{edit:false,add:false,view:false,del:false,search:false},{height:280,reloadAfterSubmit:false,closeAfterEdit:true,recreateForm:true,checkOnSubmit:true},{},{},{},{});
               
               
                myGrid.jqGrid('filterToolbar', {stringResult: true, searchOnEnter: true, defaultSearch : "cn"});
                
                //$("#del_orders").insertAfter("#add_orders");
            
        $( "#dialog-form" ).dialog({
            autoOpen: false,
            height: 'auto',
            width: '55%',
            position:[300,25],
            modal: true,
            buttons: {
                
                Cancel: function() {

                    $( this ).dialog( "close" );
                }
            },
            open: function(event,ui){
                var invoice_id = myGrid.getGridParam('selrow');
                $("#payments").jqGrid({
                    url:'index.php/procurement/populatePayments',
                    datatype: 'json',
                    mtype: 'POST',
                    postData:{invoiceId: myGrid.getGridParam('selrow')},
                    colNames:['Payment Reference','Payment Type','Amount','Comments'],
                    colModel :[ 
                        {name:'payment_reference', index:'payment_reference',editable:false},
                        {name:'payment_type', index:'payment_type',editable:false, width:160, align:'right'},
                        {name:'amount', index:'amount', editable:false,width:60, align:'right'},
                        {name:'comments', index:'comments', editable:false,width:200, align:'right'}

                    //                                            {name:'comments', index:'comments',editable:true, width:180, align:'right'}

                    ],
                    pager: '#pagerPk',
                    rowNum:10,
                    rowList:[5,10,20],
                    sortname: 'id',
                    sortorder: 'desc',
                    viewrecords: true,
                    gridview: true,
                    ignoreCase:true,
                    rownumbers:true,
                    multiselect:true,
                    height:'auto',
                    width:'50%',
                    caption: 'Line Items',

                    jsonReader : {
                        root:"quoteitemdata",
                        page: "page",
                        total: "total",
                        records: "records",
                        cell: "dprow",
                        id: "id"
                    }

                }).navGrid("#pagerPk",{edit:false,add:false,del:false,search:false,view:true},{},{},{},{},{});
                $("#payments").jqGrid('filterToolbar', {stringResult: true, searchOnEnter: true, defaultSearch : "cn"});
                $("#payments").jqGrid('navButtonAdd','#pagerPk',{
                    caption:"", 
                    title:"Add Payment Details",
                    buttonicon:"ui-icon-plus",
                    id:"add_payments",
                    onClickButton : function () { 
                        var total_value = parseFloat($("#total_value").text());
                        var amount_paid = parseFloat($("#amount_paid").text());
                        if (total_value==amount_paid){
                            $( "#modal-error-payment" ).dialog( "open" );
                        }
                        else {
                            
                            $( "#dialog-form-item" ).data('oper','add').dialog( "open" );
                        }
                         
                     } 
                 });
                 $("#payments").jqGrid('navButtonAdd','#pagerPk',{
                    caption:"", 
                    title:"Edit Payment Details",
                    buttonicon:"ui-icon-pencil",
                    id:"edit_payments",
                    onClickButton : function () { 
                            var noOfRows = $("#payments").getGridParam('selarrrow').length;
                            if (noOfRows!=1){
                                $( "#modal-warning" ).dialog( "open" );
                            }
                            else {
                                $( "#dialog-form-item" ).data('oper','edit').dialog( "open" );
                            }
                       
                         
                     } 
                 });
                console.log(invoice_id);
                $("#invoice").text(myGrid.getCell(invoice_id,'reference'));
                $("#total_value").text(myGrid.getCell(invoice_id,'total_value'));
                $("#order_ref").text(myGrid.getCell(invoice_id,'order_reference'));
                $("#amount_paid").text(myGrid.getCell(invoice_id,'amount_paid'));
                

            },
            close: function(event,ui) {
                
                $("#estValue").text("");
                $("#curPrice").text("");
                $("#status-message-li").empty();
                
                $("#payments").jqGrid("GridUnload");
            }
        });    
        
       
        $( "#modal-error-payment" ).dialog({
            autoOpen:false,
            height: 80,
            modal: true
        });
         $( "#modal-warning" ).dialog({
            autoOpen:false,
            height: 80,
            modal: true
        });
        
        $( "#dialog-form-item" ).dialog({
            autoOpen: false,
            height: 'auto',
            width: '35%',
            position:[450,25],
            modal: true,
            buttons: {
                "Register Payment": function() {
                   var isvalid = $("#paymentForm").valid();
                   
                   var invoice_id = myGrid.getGridParam('selrow');
                   console.log ("data " +" " + invoice_id);
                   if (isvalid){
                       $.ajax({
                           url:"index.php/procurement/registerPayment",
                           type:"POST",
                           data:{
                               invoice_id:invoice_id,
                               payment_type:$("#payment_type").val(),
                               payment_ref:$("#payment_ref").val(),
                               amount:$("#amount").val(),
                               total_value:parseFloat($("#amount").val())+parseFloat($("#amount_paid_form").text()),
                               oper: $("#oper").val(),
                               payment_id:$("#payment_id").val(),
                               comments:$("#bankcomments").val(),
                               total_invoiced:parseFloat($("#total_value_form").text())
                           },
                           success:function (response){
                               console.log("ssuccess" + response)
                               $("#amount_paid").text(response)
                               $("#payments").trigger("reloadGrid");                               
                           },
                           error :function (response){
                               //console.log(response)
                               //$("#amount_paid").text(response)
                                                             
                           }
                           
                       })
                       $( this ).dialog( "close" );
                   }

                },
                Cancel: function() {

                    $( this ).dialog( "close" );
                }
            },
            open: function(event,ui){
                var oper = $(this).data("oper");
                if (oper=='add'){
                    $("#oper").val(oper);
                }
                else if (oper=='edit'){
                    var pay_id = $("#payments").getGridParam('selrow');
                    $("#oper").val(oper);
                    $("#payment_id").val(pay_id);
                    $("#payment_ref").val($("#payments").getCell(pay_id,'payment_reference'));
                    $("#payment_type").val($("#payments").getCell(pay_id,'payment_type'));
                    $("#amount").val($("#payments").getCell(pay_id,'amount'));
                    $("#payment_ref").attr('disabled',true);
                    $("#payment_type").attr('disabled',true);
                }
                $("#total_value_form").text($("#total_value").text());
                
                $("#amount_paid_form").text($("#amount_paid").text());
                
            },
            close: function(event,ui) {
                $("#paymentForm").data('validator').resetForm();
                $('#paymentForm')[0].reset();
                $("#total_value_form").text("");
                $("#amount_paid_form").text("");
                $("#payment_ref").attr("disabled",false);
                $("#payment_type").attr('disabled',false);
                $("#status-message-li").empty();
                
            }
        });
        

        $("#payment_type").change(function(){
            if ($(this).val()!='cash'){
                $("#comments").addClass("required");
                $("#payment_ref").addClass("required");
                $("#payment_ref").val("");
                $("#payment_ref").attr("disabled",false);
            }
            else {
                $("#comments").removeClass("required");
                $("#payment_ref").removeClass("required");
                $("#payment_ref").val("Cash");
                $("#payment_ref").attr("disabled",true);
            }
        })
    
    });        

        </script>
        
    </head>
     
    <body>
        <?php  $this->load->view("common/menubar"); ?>
        <div id="modal-warning" title="Warning">
            <p>Please Select One Row</p>
        </div>
        <div id="modal-error-payment" title="Error">
            <p>Total Amount Paid Is Same As Invoiced Amount. No More Payment Allowed</p>
        </div>
      
        
        <div style="display: block;height: 100%;" class="shopifine-ui-dialog ui-widget ui-widget-content ui-corner-all" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog-form">
            <?php  $this->load->view("common/message"); ?>
            <div class="table-grid">
                <h1 id="table header">Invoices To Pay</h1>
                <table id="orders"><tr><td/></tr></table> 
                <div id="pager"></div>
            </div>
            
        </div>
        <div id ="dialog-form">
           
             
            <h1 id="formHeader">Add Payments For Invoice #: <span id="invoice" name ="invoice" ></span>  </h1>   
           
                    <div id ="status-message-li" class="ui-corner-all">
                
                     </div>
                    <div class="row ">
                        <div class="column ">
                           <div class="field">
                                <div class="labeldiv">Total Value:</div>  
                                <div class="valuediv" id="total_value" name ="total_value" ></div>
                            </div>
                            
                        </div>
                        <div class="column ">
                            <div class="field">
                                <div class="labeldiv">Amount Paid :</div>  
                                <div class="valuediv" name="amount_paid" id="amount_paid" ></div>
                            </div>
                        </div> 
                        <div class="column ">
                            <div class="field">
                                <div class="labeldiv">Order Reference:</div>  
                                <div class="valuediv" name="order_ref" id="order_ref" ></div>
                            </div>
                        </div> 
                    </div>
                    
                    <div id ="status-message-li" class="ui-corner-all">
                
                    </div>
                    <div class="table-grid">
                        <h1 id="table header">Payment Details</h1>
                        <table id="payments"><tr><td/></tr></table> 
                        <div id="pagerPk"></div>
                    </div>
                    
             
        </div>
        <div id ="dialog-form-item">
            
            <h1 id="formHeader">Receive Items</h1>   
            <form id="paymentForm">
                <fieldset>
                    
                    
                    <div class="row single-column-row">
                        <div class="column quote-column single-column">
                            <div class="field">
                                <div class="labeldiv">Total Value:</div>  
                                <div class="valuediv" id="total_value_form" name="total_value_form" ></div>                               
                            </div>
                           
                        </div>                        
                    </div>
                    <div class="row single-column-row">
                        <div class="column quote-column single-column">
                            <div class="field">
                                <div class="labeldiv">Amount Paid:</div>  
                                <div class="valuediv" id="amount_paid_form" name="amount_paid_form" ></div>                               
                            </div>
                           
                        </div>                        
                    </div>
                    

                    <div class="row single-column-row">
                        <div class="column quote-column single-column">
                            <div class="field">
                                <label for="payment_type" class="labeldiv">Payment Type:</label>  
                                <select id="payment_type" name ="payment_type" class="required">
                                    <option value="">Choose..</option>
                                    <option value="cash">Cash</option>
                                    <option value="cheque">Cheque</option>
                                    <option value="draft">Draft</option>
                                    <option value="online">Online</option>
                                </select>
                                
                            </div>
                        </div>                        
                    </div>
                    <div class="row single-column-row">
                        <div class="column quote-column single-column">
                            <div class="field">
                                <label for="payment_ref" class="labeldiv">Payment Reference:</label>  
                                <input id="payment_ref" name="payment_ref" class="required"/>
                                <div id ="receipt-help" class="ui-corner-all help-message">
                                    (Provide Cheque Number/Draft Number/Online Transaction Id)
                                </div>
                            </div>
                        </div>                        
                    </div>
                    
                    
                    <div class="row single-column-row">
                        <div class="column quote-column single-column">
                            <div class="field">
                                <label for="amount" class="labeldiv">Amount:</label>  
                                <input id="amount" name="amount" />
                                
                            </div>
                        </div>                        
                    </div>
                     <div class="row single-column-row">
                        <div class="column quote-column single-column">
                            <div class="field">
                                <label for="comments" class="labeldiv">Bank Details:</label>  
                                <textarea id="bankcomments" name="bankcomments" row="5" col="40"></textarea>
                                <div id ="receipt-help" class="ui-corner-all help-message">
                                    (Provide Account # , Branch Etc:)
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <input id="oper" name="oper" type="hidden" value=""/>
                    <input id="payment_id" name="payment_id" type="hidden" value=""/>
                    
                </fieldset>
            </form>
        </div>
        <?php $this->load->view("partial/footer"); ?>  
        
</body>   
</html>



    
   