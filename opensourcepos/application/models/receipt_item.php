<?php
class Receipt_item extends Base_model
{	
	function insert($purchase_quote_data)
	{
            //$this->db->insert('invoice',$invoice_data);
            $purchase_quote_data['created_at']=date('Y-m-d H:i:s');
            $status = $this->db->insert('receipt_item',$purchase_quote_data);
            log_message('debug',$this->db->last_query());
            return $status;
	}
        
         function update($where_clause_array,$purchase_quote_data)
	{
            //$this->db->insert('purchase_order',$purchase_data);
            $purchase_quote_data['last_updated_at']=date('Y-m-d H:i:s');
            $this->db->where($where_clause_array);
            
            $status = $this->db->update('receipt_item',$purchase_quote_data);
            log_message('debug',$this->db->last_query());
            return $status;
	}
        
        function updateWithNotes($where_clause_array,$update_data,$notes_array)
	{
            //$this->db->insert('purchase_order',$purchase_data);
             //$purchase_quote_data['last_updated_at']=date('Y-m-d H:i:s');
            
            
            $update_data['last_updated_at']=date('Y-m-d H:i:s');
            $this->db->where($where_clause_array);
            if (!empty($notes_array)){
                foreach ($notes_array as $key => $value) {
                    $this->db->set($key, $value, FALSE);
                }
            }
            
            $status = $this->db->update('receipt_item',$update_data);
            log_message('debug',$this->db->last_query());
            return $status;
	}
        
       
        function getById($id){
            
            $this->db->select('*');
            $this->db->where('id',$id);
            
            $query = $this->db->get('receipt_item');
            if ($query->num_rows() > 0)
            {
                return $query->row(); 
                
            } 
        }
	
        
        function getAll($csv =false,$whereClause=null,$columns_array = null,$order_limit_clause=array(),$like_fields_array=null,$in_where_clause_array=null,$or_where_clause_array=null){
            return parent::getResults('receipt_item', $csv, $whereClause, $columns_array, $order_limit_clause, $like_fields_array, $in_where_clause_array, $or_where_clause_array);
//            $orderBy = 'id';
//            $orderDir= 'desc';
//            $startLimit = 0;
//            $limit = 1000;
//            
//            if (!empty($order_limit_clause['orderBy'])){
//                $orderBy = $order_limit_clause['orderBy'];
//            }
//            if (!empty($order_limit_clause['orderDir'])){
//                $orderDir = $order_limit_clause['orderDir'];
//            }
//            if (!empty($order_limit_clause['startLimit'])){
//                $startLimit = $order_limit_clause['startLimit'];
//            }
//            if (!empty($order_limit_clause['limit'])){
//                $limit = $order_limit_clause['limit'];
//            }
//        
//            $this->load->dbutil();  
//            if (!empty($columns_array)){
//                $this->db->select($columns_array);
//            }
//            else {
//                $this->db->select('*');
//            }
//            
//            if (!empty($whereClause)){
//                $this->db->where($whereClause);
//            }
//            
//            if (!empty($or_where_clause_array)){
//                $this->db->or_where($or_where_clause_array);
//            }
//            
//            if (!empty($in_where_clause_array)){
//                $this->db->where_in($in_where_clause_array['field_name'],$in_where_clause_array['id_array']);
//            }
//            
//            if (!empty($like_fields_array)){
//                $this->db->like($like_fields_array);
//            }
//            $this->db->order_by($orderBy,$orderDir);
//            $this->db->limit($limit,$startLimit);
//            $query = $this->db->get('receipt_item');
//            log_message('debug',$this->db->last_query());
//            if ($csv){
//                return $this->dbutil->csv_from_result($query);
//            }
//            return $query->result_array();
        }
        
        
        function totalNoOfRows ($where_clause_array=null,$like_fields_array=null,$or_where_clause_array=null) {
            if (!empty($where_clause_array)){
                $this->db->where($where_clause_array);
            }
            
            if (!empty($or_where_clause_array)){
                $this->db->or_where($or_where_clause_array);
            }
            
            if (!empty($like_fields_array)){
                $this->db->like($like_fields_array);
            }
            
            $this->db->from('receipt_item');
            return $this->db->count_all_results() ;
            
            
        }
        
         function exists($order_line_id,$receipt_id){
             $this->db->from('receipt_item');	
		//$this->db->join('people', 'people.person_id = employees.person_id');
            $this->db->where('order_line_id',$order_line_id);
            $this->db->where('receipt_id',$receipt_id);
            $query = $this->db->get();
            return ($query->num_rows()==1);
            
         }
         function getByOrderLineAndReceipt($order_line_id,$receipt_id){
            
            $this->db->select('*');
            $this->db->where('order_line_id',$order_line_id);
            $this->db->where('receipt_id',$receipt_id);
            
            $query = $this->db->get('receipt_item');
            if ($query->num_rows() > 0)
            {
                return $query->row_array(); 
                
            } 
        }
        
        //this function is to show reeived total value recevied qyantity from receipt item table  in order item subgrid
        
        function getTotalReceivedItemByOrderLineItemId($order_line_id){
            $this->db->select_sum('received_quantity','total_received');
            $this->db->where('order_line_id',$order_line_id);
            $query = $this->db->from("receipt_item");
            if ($query->num_rows() > 0)
            {
                return $query->row_array(); 
                
            } 
            
        }
        
         function totalAmount($receipt_id){
            $this->db->select_sum('total_value','total');
            if (!empty($receipt_id)){
                $this->db->where('receipt_id',$receipt_id);
            }
            $query =  $this->db->get('receipt_item');
            log_message('debug',$this->db->last_query());
            return $query->row()->total;
        }
	
        
	
}

?>