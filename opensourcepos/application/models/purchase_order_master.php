<?php
class Purchase_order_master extends CI_Model 
{	
	private function insert($purchase_quote_data)
	{
            $CI = & get_instance();
            $user = $CI->User->get_logged_in_employee_info();
            $last_updated_by = $user->person_id;
            $purchase_quote_data['created_at']=date('Y-m-d H:i:s');
            $purchase_quote_data['last_updated_by'] = $last_updated_by;
            $status =  $this->db->insert('purchase_order',$purchase_quote_data);
            log_message('debug','insert statement ='.$this->db->last_query());
            return $status;
	}
        
        function update($where_clause_array,$purchase_quote_data)
	{
            //$this->db->insert('purchase_order',$purchase_data);
            $CI = & get_instance();
            $user = $CI->User->get_logged_in_employee_info();
            $last_updated_by = $user->person_id;
            $purchase_quote_data['last_updated_at']=date('Y-m-d H:i:s');
            $purchase_quote_data['last_updated_by'] = $last_updated_by;
            $this->db->where($where_clause_array);
            $status =  $this->db->update('purchase_order',$purchase_quote_data);
            log_message('debug','update statement ='.$this->db->last_query());
            return $status;
	}
        
        function exists($id)
	{
		$this->db->from('purchase_order');
		$this->db->where('id',$id);
		$query = $this->db->get();

		$ifExists = $query->num_rows()==1;

		return ($ifExists);
	}
        function getById($id){
            
            $this->db->select('*');
            $this->db->where('id',$id);
            
            $query = $this->db->get('purchase_order');
            if ($query->num_rows() > 0)
            {
                return $query->row(); 
                
            } 
        }
        
        function getGridById($id,$isarray=true){
            
            $this->db->select('*');
            $this->db->where('id',$id);
            
            $query = $this->db->get('purchase_order_grid');
            if ($query->num_rows() > 0)
            {
                if($isarray){
                    return $query->row_array(); 
                }
                else {
                    return $query->row(); 
                }
                
                
            } 
        }
        function getGridByReference($reference,$isarray=true){
            
            $this->db->select('*');
            $this->db->where('reference',$reference);
            
            $query = $this->db->get('purchase_order_grid');
            if ($query->num_rows() > 0)
            {
                if($isarray){
                    return $query->row_array(); 
                }
                else {
                    return $query->row(); 
                }
                
                
            } 
        }
        
        function getGridByQuoteReference($reference,$isarray=true){
            
            $this->db->select('*');
            $this->db->where('quote_reference',$reference);
            
            $query = $this->db->get('purchase_order_grid');
            if ($query->num_rows() > 0)
            {
                if($isarray){
                    return $query->row_array(); 
                }
                else {
                    return $query->row(); 
                }
                
                
            } 
        }
        
        function getAll($csv = false,$whereClause=null,$order_limit_clause=array(),$like_fields_array=null,$in_where_clause_array=null,$or_where_clause_array=null){
            $orderBy = 'id';
            $orderDir= 'desc';
            $startLimit = 0;
            $limit = 1000;
            
            if (!empty($order_limit_clause['orderBy'])){
                $orderBy = $order_limit_clause['orderBy'];
            }
            if (!empty($order_limit_clause['orderDir'])){
                $orderDir = $order_limit_clause['orderDir'];
            }
            if (!empty($order_limit_clause['startLimit'])){
                $startLimit = $order_limit_clause['startLimit'];
            }
            if (!empty($order_limit_clause['limit'])){
                $limit = $order_limit_clause['limit'];
            }
        
            $this->load->dbutil();  
            $this->db->select('*');
           
            if (!empty($whereClause)){
                $this->db->where($whereClause);
            }
            
            
            
            if (!empty($or_where_clause_array)){
                $this->db->or_where($or_where_clause_array);
            }
            
            if (!empty($in_where_clause_array)){
                $this->db->where_in($in_where_clause_array['field_name'],$in_where_clause_array['id_array']);
            }
            
            if (!empty($like_fields_array)){
                $this->db->like($like_fields_array);
            }
            $this->db->order_by($orderBy,$orderDir);
            $this->db->limit($limit,$startLimit);
            $query = $this->db->get('purchase_order_grid');
            log_message('debug',$this->db->last_query());
            if ($csv){
                return $this->dbutil->csv_from_result($query);
            }
            return $query->result_array();
        }
        
        
        function totalNoOfRows ($where_clause_array=null,$like_fields_array=null,$or_where_clause_array=null) {
            if (!empty($where_clause_array)){
                $this->db->where($where_clause_array);
            }
            
            if (!empty($or_where_clause_array)){
                $this->db->or_where($or_where_clause_array);
            }
            
            if (!empty($like_fields_array)){
                $this->db->like($like_fields_array);
            }
            
            $this->db->from('purchase_order_grid');
            return $this->db->count_all_results() ;
            
            
        }
        
        //business logic ..use this to create quote
        
        public function createOrder($purchase_quote_data){
            $this->db->trans_start();
            $this->insert($purchase_quote_data);
            log_message('debug','insert statement ='.$this->db->last_query());
            $id = $this->db->insert_id();
            
            $where_clause = array('id'=>$id);
            $this->update($where_clause, array('reference' => 10000000 + $id));
            log_message('debug','update statement ='.$this->db->last_query());
            $this->db->trans_complete();
            return $id;
        }
}

?>