<?php
class Purchase_invoice_item extends Base_model
{	
	function insert($purchase_quote_data)
	{
            //$this->db->insert('invoice',$invoice_data);
            $purchase_quote_data['created_at']=date('Y-m-d H:i:s');
            return $this->db->insert('purchase_invoice_item',$purchase_quote_data);
	}
        
         function update($where_clause_array,$purchase_quote_data)
	{
            //$this->db->insert('purchase_order',$purchase_data);
             $purchase_quote_data['last_updated_at']=date('Y-m-d H:i:s');
            $this->db->where($where_clause_array);
            return $this->db->update('purchase_invoice_item',$purchase_quote_data);
	}
        
       
        function getById($id){
            
            $this->db->select('*');
            $this->db->where('id',$id);
            
            $query = $this->db->get('purchase_invoice_item');
            if ($query->num_rows() > 0)
            {
                return $query->row(); 
                
            } 
        }
	
        
        function getAll($csv =false,$whereClause=null,$columns_array = null,$order_limit_clause=array(),$like_fields_array=null,$in_where_clause_array=null,$or_where_clause_array=null){
            return parent::getResults('purchase_invoice_item', $csv, $whereClause, $columns_array, $order_limit_clause, $like_fields_array, $in_where_clause_array, $or_where_clause_array);
           
        }
        
        
        function totalNoOfRows ($where_clause_array=null,$like_fields_array=null,$or_where_clause_array=null) {
            return parent::totalNoOfRowsForResults('purchase_invoice_item', $where_clause_array, $like_fields_array, $or_where_clause_array);
            
            
        }
        
        
	
        
	
}

?>