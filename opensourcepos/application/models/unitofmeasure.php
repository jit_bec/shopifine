<?php
class Unitofmeasure extends CI_Model 
{	
	function insert($uom_data)
	{
            
            return $this->db->insert('unitofmeasure',$uom_data);
	}
        
        function update($name,$uom_data)
	{
            //$this->db->insert('invoice',$invoice_data);
            $this->db->where('unit_of_measure',$name);
            return $this->db->update('unitofmeasure',$uom_data);
	}
	
	
        function exists($name)
	{
		$this->db->from('unitofmeasure');
		$this->db->where('unit_of_measure',$name);
		$query = $this->db->get();

		$ifExists = $query->num_rows()==1;

		return ($ifExists);
	}
        
        
        function totalNoOfRows () {
            $query = $this->db->query("select count(*) as total from ".$this->db->dbprefix."unitofmeasure");
            if ($query->num_rows() > 0)
            {
                $row = $query->first_row(); 
                $total = $row->total;
                if ($total == null){
                    $total = 0;
                }

                return  $total;
            
            } 
            
        }
        
//        
//        function get($name)
//	{
//		$this->db->from('unitofmeasure');
//		$this->db->where('unit_of_measure',$name);
//		$query = $this->db->get();
//
//		$ifExists = $query->num_rows()==1;
//
//		return ($ifExists);
//	}
        
	
        function getAll(){
            $this->db->select('id,unit_of_measure,denom_json');
            $query = $this->db->get('unitofmeasure');
            return $query->result_array();
        }
	
        function getJson($name){
            $this->db->select('denom_json');
            $this->db->where('unit_of_measure',$name);
            $query = $this->db->get('unitofmeasure');
            if ($query->num_rows() > 0)
            {
                $row = $query->row_array(); 
                return  $row['denom_json'];
            
            } 
            return 0;
        }
}

?>