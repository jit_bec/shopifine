<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of outgoing_payment
 *
 * @author abhijit
 */
class Outgoing_payment extends CI_Model{
    function insert($insert_data)
	{
            //$this->db->insert('invoice',$invoice_data);
            $insert_data['created_at']=date('Y-m-d H:i:s');
            return $this->db->insert('outgoing_payment',$insert_data);
	}
        
         function update($where_clause_array,$update_data)
	{
            //$this->db->insert('purchase_order',$purchase_data);
            $CI = & get_instance();
            $user = $CI->User->get_logged_in_employee_info();
            $last_updated_by = $user->person_id;
            $update_data['last_updated_at']=date('Y-m-d H:i:s');
            $update_data['last_updated_by'] = $last_updated_by;
            $this->db->where($where_clause_array);
            return $this->db->update('outgoing_payment',$update_data);
	}
        
       
        function getByPaymentId($id,$isarray=true){
            
            $this->db->select('*');
            $this->db->where('id',$id);
            
            $query = $this->db->get('outgoing_payment');
            if ($query->num_rows() > 0)
            {
                if ($isarray){
                    return $query->row_array(); 
                }
                else {
                    return $query->row();
                }
                
            } 
        }
        function getByPaymentReference($ref,$isarray=true){
            
            $this->db->select('*');
            $this->db->where('payment_reference',$ref);
            
            $query = $this->db->get('outgoing_payment');
            if ($query->num_rows() > 0)
            {
                if ($isarray){
                    return $query->row_array(); 
                }
                else {
                    return $query->row();
                }
                
            } 
        }
        
        function getAll($csv = false,$whereClause=null,$order_limit_clause=array(),$like_fields_array=null,$in_where_clause_array=null,$or_where_clause_array=null){
            $this->getColumnValues($csv,$whereClause,null,$order_limit_clause,$like_fields_array,$in_where_clause_array,$or_where_clause_array);
        }
        	
       
        function getColumnValues($csv = false,$whereClause=null,$columns_array = null,$order_limit_clause=array(),$like_fields_array=null,
                $in_where_clause_array=null,$or_where_clause_array=null){
            $orderBy = 'id';
            $orderDir= 'desc';
            $startLimit = 0;
            $limit = 1000;
            
            if (!empty($order_limit_clause['orderBy'])){
                $orderBy = $order_limit_clause['orderBy'];
            }
            if (!empty($order_limit_clause['orderDir'])){
                $orderDir = $order_limit_clause['orderDir'];
            }
            if (!empty($order_limit_clause['startLimit'])){
                $startLimit = $order_limit_clause['startLimit'];
            }
            if (!empty($order_limit_clause['limit'])){
                $limit = $order_limit_clause['limit'];
            }
        
            $this->load->dbutil();  
            if (!empty($columns_array)){
                $this->db->select($columns_array);
            }
            else {
            $this->db->select('*');
            }
            
            if (!empty($whereClause)){
                $this->db->where($whereClause);
            }
            
            if (!empty($or_where_clause_array)){
                $this->db->or_where($or_where_clause_array);
            }
            
            if (!empty($in_where_clause_array)){
                $this->db->where_in($in_where_clause_array['field_name'],$in_where_clause_array['id_array']);
            }
            
            if (!empty($like_fields_array)){
                $this->db->like($like_fields_array);
            }
            $this->db->order_by($orderBy,$orderDir);
            $this->db->limit($limit,$startLimit);
            $query = $this->db->get('outgoing_payment');
            log_message('debug',$this->db->last_query());
            if ($csv){
                return $this->dbutil->csv_from_result($query);
            }
            return $query->result_array();
        }
        
        
        function totalNoOfRows ($where_clause_array=null,$like_fields_array=null,$or_where_clause_array=null) {
            if (!empty($where_clause_array)){
                $this->db->where($where_clause_array);
            }
            
            if (!empty($or_where_clause_array)){
                $this->db->or_where($or_where_clause_array);
            }
            
            if (!empty($like_fields_array)){
                $this->db->like($like_fields_array);
            }
            
            $this->db->from('outgoing_payment');
            return $this->db->count_all_results() ;
            
            
        }
        
//         function exists($order_line_id,$receipt_id){
//             $this->db->from('receipt_item');	
//		//$this->db->join('people', 'people.person_id = employees.person_id');
//            $this->db->where('order_line_id',$order_line_id);
//            $this->db->where('receipt_id',$receipt_id);
//            $query = $this->db->get();
//            return ($query->num_rows()==1);
//            
//         }
         
        
         function totalAmount($invoice_id){
            $this->db->select_sum('total_value','total');
            if (!empty($invoice_id)){
                $this->db->where('invoice_id',$invoice_id);
            }
            $query =  $this->db->get('outgoing_payment');
            log_message('debug',$this->db->last_query());
            return $query->row()->total;
        }
	
}

?>
