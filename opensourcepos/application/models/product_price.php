<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Product_Price
 *
 * @author abhijit
 */
class Product_price extends CI_Model  {
    //put your code here
    function update ($where,$stock_data){
        if (!empty($where)){
            $this->db->where($where['column'],$where['value']);
        }
        
        return $this->db->update('product_price',$stock_data);
    }
    
    function updateByBarcode($barcode,$stock_data)
    {
        //$this->db->insert('invoice',$invoice_data);
        $this->db->where('barcode',$barcode);
        return $this->db->update('product_price',$stock_data);

    }
    
    function insert($stock_data)
    {
        //$this->db->insert('invoice',$invoice_data);
        //$this->db->where('barcode',$barcode);
        return $this->db->insert('product_price',$stock_data);

    }
        
    function getAll($csv = false,$clause=array(),$isactive=null,$like_fields_array=null){
        $orderBy = 'id';
        $orderDir= 'desc';
        $startLimit = 0;
        $limit = 1000;

        if (!empty($clause['orderBy'])){
            $orderBy = $clause['orderBy'];
        }
        if (!empty($clause['orderDir'])){
            $orderDir = $clause['orderDir'];
        }
        if (!empty($clause['startLimit'])){
            $startLimit = $clause['startLimit'];
        }
        if (!empty($clause['limit'])){
            $limit = $clause['limit'];
        }

        $this->load->dbutil();

        $this->db->select('*');
        if (!empty($isactive)){
            //only add for boolean ;ignore any other value;
            if ($isactive == 1 || $isactive == 0){
                $this->db->where('isactive',$isactive);
            }

        }

        $this->db->like($like_fields_array);

        $this->db->order_by($orderBy,$orderDir);
        $this->db->limit($limit,$startLimit);
        $query = $this->db->get('product_price');
        if ($csv){
            return $this->dbutil->csv_from_result($query);
        }
        return $query->result_array();
    }
        
    function totalNoOfRows () {
        $query = $this->db->query("select count(*) as total from ".$this->db->dbprefix."product_price");
        if ($query->num_rows() > 0)
        {
            $row = $query->first_row(); 
            $lastproductid = $row->total;
            if ($lastproductid == null){
                $lastproductid = 0;
            }

            return  $lastproductid;

        } 

    }

    function getAllBarcodes(){
        $this->db->select('barcode');
        $query = $this->db->get('product_price');

        return $query->result_array();
    }
    
    function getByProductId($id){
            
            $this->db->select('*');
            $this->db->where('product_id',$id);
            
            $query = $this->db->get('product_price');
            if ($query->num_rows() > 0)
            {
                return $query->row(); 
                
            } 
        }
}

?>
