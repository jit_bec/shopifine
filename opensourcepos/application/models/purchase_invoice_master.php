<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of purchase_invoice_master
 *
 * @author abhijit
 */
class Purchase_invoice_master extends CI_Model  {
    //put your code here
    function insert($purchase_quote_data)
	{
            $CI = & get_instance();
            $user = $CI->User->get_logged_in_employee_info();
            $last_updated_by = $user->person_id;
            $purchase_quote_data['created_at']=date('Y-m-d H:i:s');
            $purchase_quote_data['last_updated_by'] = $last_updated_by;
            $status =  $this->db->insert('purchase_invoice',$purchase_quote_data);
            log_message('debug','insert statement ='.$this->db->last_query());
            return $status;
	}
        
        function update($where_clause_array,$purchase_quote_data)
	{
            //$this->db->insert('purchase_order',$purchase_data);
            $CI = & get_instance();
            $user = $CI->User->get_logged_in_employee_info();
            $last_updated_by = $user->person_id;
            $purchase_quote_data['last_updated_at']=date('Y-m-d H:i:s');
            $purchase_quote_data['last_updated_by'] = $last_updated_by;
            $this->db->where($where_clause_array);
            $status =  $this->db->update('purchase_invoice',$purchase_quote_data);
            log_message('debug','update statement ='.$this->db->last_query());
            return $status;
	}
        
        function exists($id)
	{
		$this->db->from('purchase_invoice');
		$this->db->where('id',$id);
		$query = $this->db->get();

		$ifExists = $query->num_rows()==1;

		return ($ifExists);
	}
        function getById($id,$isarray=false){
            
            $this->db->select('*');
            $this->db->where('id',$id);
            
            $query = $this->db->get('purchase_invoice');
            if ($query->num_rows() > 0)
            {
                if ($isarray){
                    return $query->row_array(); 
                }
                else {
                   return $query->row();  
                }
                
                
            } 
        }
        
        
        
        function getAll($csv = false,$whereClause=null,$order_limit_clause=array(),$like_fields_array=null,$in_where_clause_array=null,$or_where_clause_array=null){
            $orderBy = 'id';
            $orderDir= 'desc';
            $startLimit = 0;
            $limit = 1000;
            
            if (!empty($order_limit_clause['orderBy'])){
                $orderBy = $order_limit_clause['orderBy'];
            }
            if (!empty($order_limit_clause['orderDir'])){
                $orderDir = $order_limit_clause['orderDir'];
            }
            if (!empty($order_limit_clause['startLimit'])){
                $startLimit = $order_limit_clause['startLimit'];
            }
            if (!empty($order_limit_clause['limit'])){
                $limit = $order_limit_clause['limit'];
            }
        
            $this->load->dbutil();  
            $this->db->select('*');
           
            if (!empty($whereClause)){
                $this->db->where($whereClause);
            }
            
            
            
            if (!empty($or_where_clause_array)){
                $this->db->or_where($or_where_clause_array);
            }
            
            if (!empty($in_where_clause_array)){
                $this->db->where_in($in_where_clause_array['field_name'],$in_where_clause_array['id_array']);
            }
            
            if (!empty($like_fields_array)){
                $this->db->like($like_fields_array);
            }
            $this->db->order_by($orderBy,$orderDir);
            $this->db->limit($limit,$startLimit);
            $query = $this->db->get('purchase_invoice');
            log_message('debug',$this->db->last_query());
            if ($csv){
                return $this->dbutil->csv_from_result($query);
            }
            return $query->result_array();
        }
        
        
        function totalNoOfRows ($where_clause_array=null,$like_fields_array=null,$or_where_clause_array=null) {
            if (!empty($where_clause_array)){
                $this->db->where($where_clause_array);
            }
            
            if (!empty($or_where_clause_array)){
                $this->db->or_where($or_where_clause_array);
            }
            
            if (!empty($like_fields_array)){
                $this->db->like($like_fields_array);
            }
            
            $this->db->from('purchase_invoice');
            return $this->db->count_all_results() ;
            
            
        }
}

?>
