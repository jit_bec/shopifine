<?php
require_once ("secure_area.php");

class Procurement extends Secure_area 
{
        private $user;
        private $username;
        
	function __construct()
	{
    		parent::__construct('procurement');
                $this->load->model('Purchase_order_master');
                $this->load->model('Purchase_invoice_master');
                $this->load->model('Purchase_invoice_item');
                $this->load->model('Purchase_order_item');
                $this->load->model('Purchase_quote_master');
                $this->load->model('Purchase_quote_item');
                $this->load->model('Receipt_master');
                $this->load->model('Receipt_item');
                $this->load->model('Delivery_point');
                $this->load->model('Product_price');
                $this->load->model('Outgoing_payment');
//                $this->load->model('acl/User','Userview');
                $this->user= $this->User->get_logged_in_employee_info();
                $this->username = $this->user->last_name." ".$this->user->first_name;
                $param = array('user' => $this->user->username);
                $this->load->library('acl-library/Acl',$param,'Acl');
                
	}
        
        /* PURCHASE QUOTE GRID START */
        
	function index()
	{
            $users = $this->User->getAllUsersByRole(null,array('field_name'=>'role_name',array('Everyone','Guest')));
            
            foreach($users as $user) { 
                $user_id=$user["person_id"]; 
                $username=$user["username"]; 
                //$value = $denom["denom_json"];
                if (!empty($user_id)){
                    $userOptions.="<OPTION VALUE=\"$user_id\">".$username; 
                }             
            } 
            $data['userOptions'] = $userOptions;
            $this->load->view("procurement/quote/purchase_quote_grid",$data);
	}
        
        
       /* common functions */
        function populateSuppliers(){
            $suppliers= $this->Supplier->getAll();
            foreach($suppliers as $supplier) { 

                $id=$supplier["id"]; 
                $thing=$supplier["supplier_name"]; 
                $supplieroptions.="<OPTION VALUE=\"$id\">".$thing; 
            } 
            echo $supplieroptions;
        }
        
        function getCostPrice(){
            $id = $_REQUEST['productid'];
            $productDetails = $this->Product_price->getByProductId($id);
            echo $productDetails->cost_price;
        }
        
        /* end common */
        
        /*start quote */
        
        function populateRFQ(){
           $page = $_REQUEST['page'];
           $limit = $_REQUEST['rows'];
           $sidx = $_REQUEST['sidx'];
           $sord = $_REQUEST['sord'];
           $searchOn = $_REQUEST['_search'];
           $status = $_REQUEST['status'];
           //$where = array('status'=>'rfq');
           if ($status!=self::ALL){
               $where['owner_id'] = $this->user->person_id;
               $where['status'] = $status;
           }
           
           
            
           //standard response parameters 
           $quotedata = array();
           $count = $this->Purchase_quote_master->totalNoOfRows($where);
           if( $count > 0 && $limit > 0) { 
               $total_pages = ceil($count/$limit); 
           } else { 
               $total_pages = 0; 
           } 
           if ($page > $total_pages) $page=$total_pages;

           $start = $limit*$page - $limit;
 
            // if for some reasons start position is negative set it to 0 
            // typical case is that the user type 0 for the requested page 
           if($start <0) $start = 0; 
           $clauses = array('orderBy'=>$sidx,'orderDir'=>$sord,'startLimit'=>$start,'limit'=>$limit);
            
           $data['total'] = $total_pages;
           $data['page'] = $page;
           $data['records'] = $count; 
           if($searchOn=='true') {
                
                $filters = json_decode($_REQUEST['filters'],true);
                $groupOp = $filters['groupOp'];
                $rules = $filters['rules'];
                $like_condition = array();
                foreach ($rules as $rule){
                    $field = $rule['field'];
                    $op= $rule['op'];
                    $input = $rule['data'];
                    $like_condition[$field] = trim($input);
                }
                $quotes = $this->Purchase_quote_master->getAll(false,$where,$clauses,$like_condition);
            }
            else {
                $quotes = $this->Purchase_quote_master->getAll(false,$where);
            }
           
           foreach ($quotes as $dp){
               if ($status==self::WAITING_FOR_APPROVAL){
                   /* we dont need actions column in approval grid . So not passing the blank */
                   array_push($quotedata, array('id'=> $dp['id'],'dprow' => array($dp['reference'],$dp['supplier_name'],$dp['estimated_value'],$dp['status'],$dp['raised_by_name'],$dp['owner_name'],$dp['needed_by_date'])));
               }
               else if ($status==self::REJECTED){
                   /* Add Extra Columns Rejected and Rejected Notes Remove Status */
                   array_push($quotedata, array('id'=> $dp['id'],'dprow' => array($dp['reference'],$dp['supplier_name'],$dp['estimated_value'],$dp['raised_by_name'],$dp['owner_name'],$dp['needed_by_date'],$dp['approved_by_name'],$dp['approval_notes'])));
               }
               else if ($status==self::OPEN){
                   array_push($quotedata, array('id'=> $dp['id'],'dprow' => array('',$dp['reference'],$dp['supplier_name'],$dp['estimated_value'],$dp['status'],$dp['raised_by_name'],$dp['owner_name'],$dp['needed_by_date'])));
               }
               else {
                   array_push($quotedata, array('id'=> $dp['id'],'dprow' => array($dp['reference'],$dp['supplier_name'],$dp['estimated_value'],$dp['status'],$dp['raised_by_name'],$dp['owner_name'],$dp['needed_by_date'],$dp['notes'],$dp['approved_by_name'],$dp['approval_notes'])));
               }
           }
           $data['quotedata'] = $quotedata;
           echo json_encode($data);
        }
        
        
        function createRFQ(){
            $id = $_REQUEST['quoteId'];
            $purchase_quote_data['supplier_id'] = $_REQUEST['supplierId'];
            $purchase_quote_data['warehouse_id'] = $_REQUEST['warehouseId'];
            $dateObj = DateTime::createFromFormat('d/m/Y', $_REQUEST['reqdate']);
            log_message('debug','converted '.$dateObj->format('Y-m-d'));
            $purchase_quote_data['needed_by_date'] = $dateObj->format('Y-m-d');
            $purchase_quote_data['notes'] = $_REQUEST['notes'];
            $purchase_quote_data['owner_id'] = $this->user->person_id;
            
            if (empty($id)){
                $purchase_quote_data['raised_by'] = $this->user->person_id;
                $id =  $this->Purchase_quote_master->createQuote($purchase_quote_data);
            }
            else {
                $where_clause = array('id'=>$id);
                $this->Purchase_quote_master->update($where_clause, $purchase_quote_data);
            }
            echo $id;
        }
        //this function validates the record after close button of dialog being pressed. If no items are aadded the record is deleted
        function closeValidate(){
            $id = $_REQUEST['id'];   
            
            if (!empty($id)){
                $ifExists = $this->Purchase_quote_master->exists($id);
                if ($ifExists){

                    $where_clause_quote = array('id'=>$id);
                    $this->Purchase_quote_master->update($where_clause_quote,array('status'=>self::CANCELLED));

                    $where_clause_quote_item = array('quote_id'=>$id);
                    $this->Purchase_quote_item->update($where_clause_quote_item,array('status'=>self::CANCELLED));
                }
            }
           
            
        }
        
        function modifyQuote (){
            $id = $_REQUEST['id'];
            
            $oper = $_REQUEST['oper'];
            $this->db->trans_start();
            if ($oper=='edit'){
                //$dateObj = DateTime::createFromFormat('d/m/Y', $_REQUEST['needed_by_date']);
                //log_message('debug','converted item date '.$dateObj->format('Y-m-d'));
                $purchase_quote_data['needed_by_date'] = $_REQUEST['needed_by_date'];
                //while editing the supplier id is passed.
                $purchase_quote_data['supplier_id'] = $_REQUEST['supplier_name'];
                $where_clause_quote = array('id'=>$id);
                
                $this->Purchase_quote_master->update($where_clause_quote,$purchase_quote_data);
                
            }
            else if ($oper=='del'){
                $idAraay =  explode(",", $id);
                foreach($idAraay as $tempId){
                    $where_clause_quote = array('id'=>$tempId);
                    $this->Purchase_quote_master->update($where_clause_quote,array('status'=>'cancelled'));

                    $where_clause_quote_item = array('quote_id'=>$tempId);
                    $this->Purchase_quote_item->update($where_clause_quote_item,array('status'=>'cancelled'));
                }
                
            }
            $this->db->trans_complete();
        }
//        
	function testdate(){
//            $date = '25/10/2012';
//            $dateObj = DateTime::createFromFormat('d/m/Y', $date);
//            $mysqldt =  $dateObj->format('Y-m-d');
            echo date("Y-m-d H:i:s", time());
        }
        
        function addQuoteItem (){
            //$id = $_REQUEST['quoteId'];
            $product_id = $_REQUEST['productid'];
            $purchase_quote_data['quote_id'] = $_REQUEST['quoteid'];
            $purchase_quote_data['product_id'] = $product_id;
            $dateObj = DateTime::createFromFormat('d/m/Y', $_REQUEST['needeedByDate']);
            log_message('debug','converted item date '.$dateObj->format('Y-m-d'));
            $purchase_quote_data['needed_by_date'] = $dateObj->format('Y-m-d');
            $purchase_quote_data['quoted_quantity'] = $_REQUEST['quantity'];
            $purchase_quote_data['expected_price'] = $_REQUEST['exprice'];
            $purchase_quote_data['estimated_value'] = $_REQUEST['quantity']*$_REQUEST['exprice'];
            $purchase_quote_data['comments'] = $_REQUEST['descItem'];
            $productDetails = $this->Product->getByProductId($product_id);
            $purchase_quote_data['sku'] = $productDetails->barcode;
            $purchase_quote_data['name'] = $productDetails->product_name;
            $id =  $this->createQuoteItem($purchase_quote_data);
        }
        //business logic
        function createQuoteItem($purchase_quote_data){
            $this->db->trans_start();
            /* insert into quote item */
            
            $this->Purchase_quote_item->insert($purchase_quote_data);
            log_message('debug','insert statement ='.$this->db->last_query());
            $id = $this->db->insert_id();
            /* end insert  */
            
            /* update reference number in  quote item */
            $where_clause = array('id'=>$id);
            $this->Purchase_quote_item->update($where_clause, array('reference' => 10000000 + $id));
            /* end update */
            
            /* update estimated value in quote master */
            $quote_id = $purchase_quote_data['quote_id'];
            $quote_details=$this->Purchase_quote_master->getById($quote_id);
            $estimated_value = $quote_details->estimated_value + $purchase_quote_data['estimated_value'];
            $this->Purchase_quote_master->update(array('id'=>$quote_id),array('estimated_value'=>$estimated_value));
            log_message('debug','update statement ='.$this->db->last_query());
            /* end update estimated value in quote master */
            
            $this->db->trans_complete();
            return $id;
        }
        function modifyQuoteItem (){
            $id = $_REQUEST['id'];
            $item_details=$this->Purchase_quote_item->getById($id);
            $quote_id = $item_details->quote_id;
            $current_est_value = $item_details->estimated_value;
            $oper = $_REQUEST['oper'];
            $this->db->trans_start();
            if ($oper=='edit'){
                //$dateObj = DateTime::createFromFormat('d/m/Y', $_REQUEST['needed_by_date']);
                //log_message('debug','converted item date '.$dateObj->format('Y-m-d'));
                $purchase_quote_data['needed_by_date'] = $_REQUEST['needed_by_date'];
                $purchase_quote_data['quoted_quantity'] = $_REQUEST['quoted_quantity'];
                $purchase_quote_data['expected_price'] = $_REQUEST['expected_price'];
                $purchase_quote_data['comments'] = $_REQUEST['comments'];
                $purchase_quote_data['estimated_value'] = $_REQUEST['quoted_quantity']*$_REQUEST['expected_price'];

                $where_clause = array('id'=>$id);
                
                $this->Purchase_quote_item->update($where_clause,$purchase_quote_data);
                
                $quote_details=$this->Purchase_quote_master->getById($quote_id);
                $estimated_value = $quote_details->estimated_value - $current_est_value + $purchase_quote_data['estimated_value'];
                $this->Purchase_quote_master->update(array('id'=>$quote_id),array('estimated_value'=>$estimated_value));
            }
            else if ($oper=='del'){
                $quote_details=$this->Purchase_quote_master->getById($quote_id);
                $estimated_value = $quote_details->estimated_value - $item_details->estimated_value;
                $this->Purchase_quote_master->update(array('id'=>$quote_id),array('estimated_value'=>$estimated_value));
                $where_clause = array('id'=>$id);
                
                $this->Purchase_quote_item->update($where_clause,array('status'=>'cancelled'));
            }
            $this->db->trans_complete();
            
        }
        
        function populateQuoteItems(){
            $quoteid = $_REQUEST['quoteId'];
            if (!empty($quoteid)){
                $page = $_REQUEST['page'];
                $limit = $_REQUEST['rows'];
                $sidx = $_REQUEST['sidx'];
                $sord = $_REQUEST['sord'];

                //standard response parameters 

                $quotedata = array();
                $where = array('quote_id' => $quoteid );
                $count = $this->Purchase_quote_item->totalNoOfRows($where);
                if( $count > 0 && $limit > 0) { 
                    $total_pages = ceil($count/$limit); 
                } else { 
                    $total_pages = 0; 
                } 
                if ($page > $total_pages) $page=$total_pages;

                $start = $limit*$page - $limit;

                 // if for some reasons start position is negative set it to 0 
                 // typical case is that the user type 0 for the requested page 
                if($start <0) $start = 0; 
                $clauses = array('orderBy'=>$sidx,'orderDir'=>$sord,'startLimit'=>$start,'limit'=>$limit);

                $data['total'] = $total_pages;
                $data['page'] = $page;
                $data['records'] = $count; 

                $quotes = $this->Purchase_quote_item->getAll(false,$where);

                foreach ($quotes as $dp){
                    array_push($quotedata, array('id'=> $dp['id'],'dprow' => array($dp['name'],$dp['quoted_quantity'],$dp['needed_by_date'],$dp['expected_price'],$dp['estimated_value'],$dp['comments'])));
                }
                $data['quoteitemdata'] = $quotedata;
                echo json_encode($data);
           }
           
        }
        
        function generatePOFromRFQ(){
            $ids= $_REQUEST['ids'];
            
            foreach($ids as $id){
                
                log_message('debug',' id '.$id);
                $quote_details=$this->Purchase_quote_master->getById($id,true);
                if ($quote_details['status']==self::OPEN ||$quote_details['status']==self::WAITING_FOR_APPROVAL ) {
                    $this->db->trans_start();
                    $this->Purchase_quote_master->update(array('id'=>$id),array('approved_by'=>$this->user->person_id));
                    $this->generatePOInternal($quote_details);
                    $this->db->trans_complete();
                }
                 
            }
           
        }
        /* PURCHASE QUOTE GRID END*/
        
        /* APPROVAL GRUD START */
        
        function approveOrReject(){
            $id= $_REQUEST['quoteId'];
            $action= $_REQUEST['action'];
            log_message('debug',' id '.$id);
            log_message('debug',' action '.$action);
            $quote_details=$this->Purchase_quote_master->getById($id,true);
            if ($quote_details['status']==self::WAITING_FOR_APPROVAL ) {
                if ($action == 'approve'){
                    $this->db->trans_start();
                    $this->Purchase_quote_master->update(array('id'=>$id),array('approved_by'=>$this->user->person_id,'approval_notes'=>$_REQUEST['quote_approval_notes']));
                    $this->generatePOInternal($quote_details);
                    $this->db->trans_complete();
                }
                else if ($action == 'reject'){
                    $this->db->trans_start();
                    $this->Purchase_quote_master->update(array('id'=>$id),array('status'=>self::REJECTED,'approved_by'=>$this->user->person_id,'approval_notes'=>$_REQUEST['quote_approval_notes']));
                    $item_details=$this->Purchase_quote_item->getByQuoteId($id);
                    foreach ($item_details as $item_data){
                        
                            $this->Purchase_quote_item->update(array('id'=>$item_data['id']),array('status'=>self::REJECTED));
                        
                    }
                    $this->db->trans_complete();
                }
                
            }
 
        }
        
        /* APPROVAL GRID END */
        
        /* REJECTED GRID START*/
        function reopenRFQ (){
            $ids= $_REQUEST['ids'];
            
            foreach($ids as $id){
                
                log_message('debug',' id '.$id);
                $quote_details=$this->Purchase_quote_master->getById($id,true);
                if ($quote_details['status']==self::REJECTED ) {
                    $this->db->trans_start();
                    $this->Purchase_quote_master->update(array('id'=>$id),array('status'=>self::OPEN,'last_updated_by'=>$this->user->person_id));
                    $item_details=$this->Purchase_quote_item->getByQuoteId($id);
                    foreach ($item_details as $item_data){
                        
                            $this->Purchase_quote_item->update(array('id'=>$item_data['id']),array('status'=>self::OPEN));
                        
                    }
                    $this->db->trans_complete();
                }
                 
            }
        }
        /* REJECTED GRID END*/
        
        /* COMMON FUNCTIONS FOR RFQ GRIDS  */
        private function generatePOInternal($quote_details){
            
            $id=$quote_details['id'];
            $order_data['owner_id'] = $quote_details['owner_id'];
            $order_data['needed_by_date'] = $quote_details['needed_by_date'];
            $order_data['generated_by'] = $this->user->person_id;
            $order_data['quote_id'] = $quote_details['id'];
            log_message('debug',json_encode($order_data));
            $order_id = $this->Purchase_order_master->createOrder($order_data);
            log_message('debug',$this->db->last_query());
            $this->Purchase_quote_master->update(array('id'=>$id),array('status'=>self::ORDERED,'order_id'=>$order_id));
            log_message('debug',$this->db->last_query());
            //now update line item status
            $item_details=$this->Purchase_quote_item->getByQuoteId($id);
            foreach ($item_details as $item_data){
                if ($item_data['status'] == self::OPEN || $item_data['status'] == self::WAITING_FOR_APPROVAL){
                    $order_item_data['name'] = $item_data['name'];
                    $order_item_data['sku'] = $item_data['sku'];
                    $order_item_data['product_id'] = $item_data['product_id'];
                    $order_item_data['estimated_value'] = $item_data['estimated_value'];
                    $order_item_data['needed_by_date'] = $item_data['needed_by_date'];
                    $order_item_data['quoted_quantity'] = $item_data['quoted_quantity'];
                    //repeatative ..as of now as no difference between quoted and ordered quantity
                    $order_item_data['ordered_quantity'] = $item_data['quoted_quantity'];
                    $order_item_data['expected_price'] = $item_data['expected_price'];
                    $order_item_data['order_id'] =$order_id;
                    $order_item_data['quote_line_id'] = $item_data['id'];
                    $order_line_id = $this->createOrderItem($order_item_data);
                    $this->Purchase_quote_item->update(array('id'=>$item_data['id']),array('status'=>self::ORDERED,'order_line_id'=>$order_line_id));
                }
            }
            
        }
        
        /*end quote */
        
        /* RECEIVING GRID START */
        
        function populatePOToReceive(){
            $person_id = $this->user->person_id;
            $in_where=array('field_name'=>'status','id_array'=>array(self::OPEN,self::RECEIVING)) ;
            $this->populatePOInternal($person_id,$in_where);
            
        }
        
        /* RECEIVING GRID END */
        
        function populatePayments(){
            $invoiceId = $_REQUEST['invoiceId'];
            if (!empty($invoiceId)){
                $page = $_REQUEST['page'];
                $limit = $_REQUEST['rows'];
                $sidx = $_REQUEST['sidx'];
                $sord = $_REQUEST['sord'];

                //standard response parameters 

                $quotedata = array();
                $where = array('invoice_id' => $invoiceId );
                $count = $this->Outgoing_payment->totalNoOfRows($where);
                if( $count > 0 && $limit > 0) { 
                    $total_pages = ceil($count/$limit); 
                } else { 
                    $total_pages = 0; 
                } 
                if ($page > $total_pages) $page=$total_pages;

                $start = $limit*$page - $limit;

                 // if for some reasons start position is negative set it to 0 
                 // typical case is that the user type 0 for the requested page 
                if($start <0) $start = 0; 
                $clauses = array('orderBy'=>$sidx,'orderDir'=>$sord,'startLimit'=>$start,'limit'=>$limit);

                $data['total'] = $total_pages;
                $data['page'] = $page;
                $data['records'] = $count; 

                $quotes = $this->Outgoing_payment->getAll(false,$where);

                foreach ($quotes as $dp){
                    array_push($quotedata, array('id'=> $dp['id'],'dprow' => array($dp['payment_reference'],$dp['payment_type'],$dp['amount'],$dp['comments'])));
                }
                $data['quoteitemdata'] = $quotedata;
                echo json_encode($data);
            }
        }
        
        
        function populateReceived(){
           $person_id = $this->user->person_id;
           
           $in_where=array('field_name'=>'status','id_array'=>array(self::RECEIVED)) ;
           // for Invoices to Pay Grid ..Override Status
           $invoice_id = $_REQUEST['invoiceId'];
           if (!empty($invoice_id)){
               $where['invoice_id'] = $invoice_id;
               $in_where=array('field_name'=>'status','id_array'=>array(self::INVOICED)) ;
           }
           
           
           $page = $_REQUEST['page'];
           $limit = $_REQUEST['rows'];
           $sidx = $_REQUEST['sidx'];
           $sord = $_REQUEST['sord'];
           $searchOn = $_REQUEST['_search'];
           
           if (!empty($person_id)){
                $where['owner_id'] = $person_id;
           }
           
           //standard response parameters 
           $quotedata = array();
           $count = $this->Receipt_master->totalNoOfRows($where);
           if( $count > 0 && $limit > 0) { 
               $total_pages = ceil($count/$limit); 
           } else { 
               $total_pages = 0; 
           } 
           if ($page > $total_pages) $page=$total_pages;

           $start = $limit*$page - $limit;
 
            // if for some reasons start position is negative set it to 0 
            // typical case is that the user type 0 for the requested page 
           if($start <0) $start = 0; 
           $clauses = array('orderBy'=>$sidx,'orderDir'=>$sord,'startLimit'=>$start,'limit'=>$limit);
            
           $data['total'] = $total_pages;
           $data['page'] = $page;
           $data['records'] = $count; 
           if($searchOn=='true') {
                
                $filters = json_decode($_REQUEST['filters'],true);
                $groupOp = $filters['groupOp'];
                $rules = $filters['rules'];
                $like_condition = array();
                foreach ($rules as $rule){
                    $field = $rule['field'];
                    $op= $rule['op'];
                    $input = $rule['data'];
                    if ($field == 'order_reference' ){
                        $orderDetails = $this->Purchase_order_master->getGridByReference($input);
                        $like_condition['order_id'] = $orderDetails['id'];
                    }
                    else if ($field == 'quote_reference'){
                        $orderDetails = $this->Purchase_order_master->getGridByQuoteReference($input);
                        $like_condition['order_id'] = $orderDetails['id'];
                    }
                    else if ($field == 'supplier_name'){
                        $supplierDetails = $this->Supplier->getByName($input);
                        $like_condition['supplier_id'] = $supplierDetails['id'];
                    }
                    else {
                        $like_condition[$field] = trim($input);
                    }
                    
                }
                $quotes = $this->Receipt_master->getAll(false,$where,null,$clauses,$like_condition,$in_where);
            }
            else {
                $quotes = $this->Receipt_master->getAll(false,$where,null,null,null,$in_where);
            }
           
           foreach ($quotes as $dp){
               $orderDetails = $this->Purchase_order_master->getGridById($dp['order_id']);
               array_push($quotedata, array('id'=> $dp['id'],'dprow' => array($dp['reference'],$dp['supplier_receipt_number'],$orderDetails['supplier_name'],$dp['status'],$dp['order_id'],$orderDetails['reference'],$orderDetails['quote_reference'])));
           }
           $data['quotedata'] = $quotedata;
           echo json_encode($data);
        }
        
       
        
        function populatePOInternal($person_id,$in_where){
           $page = $_REQUEST['page'];
           $limit = $_REQUEST['rows'];
           $sidx = $_REQUEST['sidx'];
           $sord = $_REQUEST['sord'];
           $searchOn = $_REQUEST['_search'];
           
           if (!empty($person_id)){
                $where['owner_id'] = $person_id;
           }
           
           //standard response parameters 
           $quotedata = array();
           $count = $this->Purchase_order_master->totalNoOfRows($where);
           if( $count > 0 && $limit > 0) { 
               $total_pages = ceil($count/$limit); 
           } else { 
               $total_pages = 0; 
           } 
           if ($page > $total_pages) $page=$total_pages;

           $start = $limit*$page - $limit;
 
            // if for some reasons start position is negative set it to 0 
            // typical case is that the user type 0 for the requested page 
           if($start <0) $start = 0; 
           $clauses = array('orderBy'=>$sidx,'orderDir'=>$sord,'startLimit'=>$start,'limit'=>$limit);
            
           $data['total'] = $total_pages;
           $data['page'] = $page;
           $data['records'] = $count; 
           if($searchOn=='true') {
                
                $filters = json_decode($_REQUEST['filters'],true);
                $groupOp = $filters['groupOp'];
                $rules = $filters['rules'];
                $like_condition = array();
                foreach ($rules as $rule){
                    $field = $rule['field'];
                    $op= $rule['op'];
                    $input = $rule['data'];
                    $like_condition[$field] = trim($input);
                }
                $quotes = $this->Purchase_order_master->getAll(false,$where,$clauses,$like_condition,$in_where);
            }
            else {
                $quotes = $this->Purchase_order_master->getAll(false,$where,null,null,$in_where);
            }
           
           foreach ($quotes as $dp){
               array_push($quotedata, array('id'=> $dp['id'],'dprow' => array($dp['reference'],$dp['supplier_id'],$dp['supplier_name'],$dp['estimated_value'],$dp['status'],$dp['raised_by_name'],$dp['needed_by_date'])));
           }
           $data['quotedata'] = $quotedata;
           echo json_encode($data);
        }
        
        
        function validateReceiving(){
            $order_id = $_REQUEST['order_id'];
            $receiving_notes = $_REQUEST['receiving_notes'];
            //check the no of order line items with status receiving. 
            $totalNoOfRowsByOrderIdReceived = $this->Purchase_order_item->totalNoOfRows(array('order_id'=>$order_id,'status'=>self::RECEIVED));
             $totalNoOfRowsByOrderIdTotal = $this->Purchase_order_item->totalNoOfRows(array('order_id'=>$order_id));
             if ($totalNoOfRowsByOrderIdReceived == $totalNoOfRowsByOrderIdTotal){
                 // all the order lines are received       
                 
                 $this->Purchase_order_master->update(array('id'=>$order_id,'status'=>self::RECEIVING),array('status'=>self::RECEIVED,'receiving_notes'=>$receiving_notes));
                 $this->Receipt_master->update(array('order_id'=>$order_id,'status'=>self::RECEIVING),array('status'=>self::RECEIVED));
             }
             else {
                  $this->Purchase_order_master->update(array('id'=>$order_id,'status'=>self::RECEIVING),array('receiving_notes'=>$receiving_notes));
             }
            

        }
//        function validateReceivingc
//            $order_id = $_REQUEST['order_id'];
//            $receiving_notes = $_REQUEST['receiving_notes'];
//            //check the no of order line items with status receiving. 
//            $totalNoOfRowsByOrderIdReceived = $this->Purchase_order_item->totalNoOfRows(array('order_id'=>$order_id,'status'=>self::RECEIVING));
//             $totalNoOfRowsByOrderIdOpen = $this->Purchase_order_item->totalNoOfRows(array('order_id'=>$order_id,'status'=>self::OPEN));
//             if ($totalNoOfRowsByOrderIdReceiving == 0 && $totalNoOfRowsByOrderIdOpen==0){
//                 // none of the line items are in Open or receiving State        
//                 
//                 $this->Purchase_order_master->update(array('id'=>$order_id,'status'=>self::RECEIVING),array('status'=>self::RECEIVED,'receiving_notes'=>$receiving_notes));
//                 $this->Receipt_master->update(array('order_id'=>$order_id,'status'=>self::RECEIVING),array('status'=>self::RECEIVED));
//             }
//             else {
//                  $this->Purchase_order_master->update(array('id'=>$order_id,'status'=>self::RECEIVING),array('receiving_notes'=>$receiving_notes));
//             }
//            
//
//        }
        
        function modifyOrder (){
            $id = $_REQUEST['id'];
            
            $oper = $_REQUEST['oper'];
            $this->db->trans_start();
            if ($oper=='edit'){
                //$dateObj = DateTime::createFromFormat('d/m/Y', $_REQUEST['needed_by_date']);
                //log_message('debug','converted item date '.$dateObj->format('Y-m-d'));
                $purchase_quote_data['needed_by_date'] = $_REQUEST['needed_by_date'];
                $purchase_quote_data['supplier_name'] = $_REQUEST['supplier_name'];
                $where_clause_quote = array('id'=>$id);
                
                $this->Purchase_quote_master->update($where_clause_quote,$purchase_quote_data);
                
            }
            else if ($oper=='del'){
                $idAraay =  explode(",", $id);
                foreach($idAraay as $tempId){
                    $where_clause_quote = array('id'=>$tempId);
                    $this->Purchase_quote_master->update($where_clause_quote,array('status'=>'cancelled'));

                    $where_clause_quote_item = array('quote_id'=>$tempId);
                    $this->Purchase_quote_item->update($where_clause_quote_item,array('status'=>'cancelled'));
                }
                
            }
            $this->db->trans_complete();
        }
        
        function addOrderItem (){
            //$id = $_REQUEST['quoteId'];
            $product_id = $_REQUEST['productid'];
            $purchase_quote_data['quote_id'] = $_REQUEST['orderId'];
            $purchase_quote_data['product_id'] = $product_id;
            $dateObj = DateTime::createFromFormat('d/m/Y', $_REQUEST['needeedByDate']);
            log_message('debug','converted item date '.$dateObj->format('Y-m-d'));
            $purchase_quote_data['needed_by_date'] = $dateObj->format('Y-m-d');
            $purchase_quote_data['quoted_quantity'] = $_REQUEST['quantity'];
            $purchase_quote_data['expected_price'] = $_REQUEST['exprice'];
            $purchase_quote_data['estimated_value'] = $_REQUEST['quantity']*$_REQUEST['exprice'];
            $purchase_quote_data['comments'] = $_REQUEST['descItem'];
            $productDetails = $this->Product->getByProductId($product_id);
            $purchase_quote_data['sku'] = $productDetails->barcode;
            $purchase_quote_data['name'] = $productDetails->product_name;
            $id =  $this->createQuoteItem($purchase_quote_data);
        }
        //business logic
        function createOrderItem($purchase_quote_data){
            $this->db->trans_start();
            /* insert into quote item */
            
            $this->Purchase_order_item->insert($purchase_quote_data);
            log_message('debug','insert statement ='.$this->db->last_query());
            $id = $this->db->insert_id();
            /* end insert  */
            
            /* update reference number in  quote item */
            $where_clause = array('id'=>$id);
            $this->Purchase_order_item->update($where_clause, array('reference' => 10000000 + $id));
            /* end update */
            
            $this->db->trans_complete();
            return $id;
        }
        function modifyOrderItem (){
            $id = $_REQUEST['id'];
            $item_details=$this->Purchase_quote_item->getById($id);
            $quote_id = $item_details->quote_id;
            $current_est_value = $item_details->estimated_value;
            $oper = $_REQUEST['oper'];
            $this->db->trans_start();
            if ($oper=='edit'){
                //$dateObj = DateTime::createFromFormat('d/m/Y', $_REQUEST['needed_by_date']);
                //log_message('debug','converted item date '.$dateObj->format('Y-m-d'));
                $purchase_quote_data['needed_by_date'] = $_REQUEST['needed_by_date'];
                $purchase_quote_data['quoted_quantity'] = $_REQUEST['quoted_quantity'];
                $purchase_quote_data['expected_price'] = $_REQUEST['expected_price'];
                $purchase_quote_data['comments'] = $_REQUEST['comments'];
                $purchase_quote_data['estimated_value'] = $_REQUEST['quoted_quantity']*$_REQUEST['expected_price'];

                $where_clause = array('id'=>$id);
                
                $this->Purchase_quote_item->update($where_clause,$purchase_quote_data);
                
                $quote_details=$this->Purchase_quote_master->getById($quote_id);
                $estimated_value = $quote_details->estimated_value - $current_est_value + $purchase_quote_data['estimated_value'];
                $this->Purchase_quote_master->update(array('id'=>$quote_id),array('estimated_value'=>$estimated_value));
            }
            else if ($oper=='del'){
                $quote_details=$this->Purchase_quote_master->getById($quote_id);
                $estimated_value = $quote_details->estimated_value - $item_details->estimated_value;
                $this->Purchase_quote_master->update(array('id'=>$quote_id),array('estimated_value'=>$estimated_value));
                $where_clause = array('id'=>$id);
                
                $this->Purchase_quote_item->update($where_clause,array('status'=>'cancelled'));
            }
            $this->db->trans_complete();
            
        }
        
        function populateOrderItems(){
            $orderid = $_REQUEST['orderId'];
            if (!empty($orderid)){
                $page = $_REQUEST['page'];
                $limit = $_REQUEST['rows'];
                $sidx = $_REQUEST['sidx'];
                $sord = $_REQUEST['sord'];

                //standard response parameters 

                $quotedata = array();
                $where = array('order_id' => $orderid );
                $count = $this->Purchase_order_item->totalNoOfRows($where);
                if( $count > 0 && $limit > 0) { 
                    $total_pages = ceil($count/$limit); 
                } else { 
                    $total_pages = 0; 
                } 
                if ($page > $total_pages) $page=$total_pages;

                $start = $limit*$page - $limit;

                 // if for some reasons start position is negative set it to 0 
                 // typical case is that the user type 0 for the requested page 
                if($start <0) $start = 0; 
                $clauses = array('orderBy'=>$sidx,'orderDir'=>$sord,'startLimit'=>$start,'limit'=>$limit);

                $data['total'] = $total_pages;
                $data['page'] = $page;
                $data['records'] = $count; 

                $quotes = $this->Purchase_order_item->getAll(false,$where);

                foreach ($quotes as $dp){
                    array_push($quotedata, array('id'=> $dp['id'],'dprow' => array($dp['product_id'],$dp['name'],$dp['quoted_quantity'],$dp['received_quantity'],$dp['returned_quantity'],$dp['needed_by_date'],$dp['expected_price'],$dp['estimated_value'],$dp['received_value'],$dp['returned_value'],$dp['comments'])));
                }
                $data['orderitemdata'] = $quotedata;
                echo json_encode($data);
           }         
           
        }
        
        function populateReceiptItems(){
            $receiptId = $_REQUEST['receiptId'];
            $orderLineId = $_REQUEST['orderLineId'];
            // as of now keep default oper as "receipt"
            $oper = "receipt";
            
            if (!empty($_REQUEST['oper'])){
                $oper = $_REQUEST['oper'];
            }
            $page = $_REQUEST['page'];
            $limit = $_REQUEST['rows'];
            $sidx = $_REQUEST['sidx'];
            $sord = $_REQUEST['sord'];

            //standard response parameters 

            $quotedata = array();
            if ($oper=="receipt"){
               $where = array('receipt_id' => $receiptId ); 
            }
            else if ($oper=="orderline"){
               $where = array('order_line_id' => $orderLineId );  
            }
            $count = $this->Receipt_item->totalNoOfRows($where);
            if( $count > 0 && $limit > 0) { 
                $total_pages = ceil($count/$limit); 
            } else { 
                $total_pages = 0; 
            } 
            if ($page > $total_pages) $page=$total_pages;

            $start = $limit*$page - $limit;

             // if for some reasons start position is negative set it to 0 
             // typical case is that the user type 0 for the requested page 
            if($start <0) $start = 0; 
            $clauses = array('orderBy'=>$sidx,'orderDir'=>$sord,'startLimit'=>$start,'limit'=>$limit);

            $data['total'] = $total_pages;
            $data['page'] = $page;
            $data['records'] = $count; 

            $quotes = $this->Receipt_item->getAll(false,$where);

            foreach ($quotes as $dp){
                $receipt_data = $this->Receipt_master->getById($dp['receipt_id'],true);
                array_push($quotedata, array('id'=> $dp['id'],'dprow' => array($receipt_data['supplier_receipt_number'],$receipt_data['reference'],
                   $dp['receipt_id'], $dp['order_line_id'],$dp['product_id'],$dp['name'],$dp['ordered_quantity'],$dp['received_quantity'],$dp['received_value'],$dp['returned_quantity'],$dp['returned_value'],$dp['receiving_notes'],$dp['returned_notes'])));
            }
            $data['receiptitemdata'] = $quotedata;
            echo json_encode($data);
                    
           
        }
        
        function getOrderDetails(){
            $orderId=$_REQUEST['orderId'];
            if (!empty($orderId)){
                $order = $this->Purchase_order_master->getGridById($orderId);
                echo json_encode($order);
            }


        }
        function getQuoteDetails(){
            $quoteId=$_REQUEST['quoteId'];
            if (!empty($quoteId)){
                $quote = $this->Purchase_quote_master->getGridById($quoteId);
                echo json_encode($quote);
            }


        }
        function loadPOGrid()
	{
            
            $this->load->view("procurement/purchaseorder/purchase_order_grid",$data);
	}
        function loadOpenInvoicesGrid()
	{
            
            $this->load->view("procurement/invoice/purchase_invoice_grid",$data);
	}
        function loadFormFragment(){
            $this->load->view("procurement/purchaseorder/po_details",$data);

        }
        function loadNotesFragment(){
            $this->load->view("procurement/purchaseorder/po_notes",$data);

        }
        
        function loadWaitingForApprovalQuotesGrid (){
            $this->load->view("procurement/quote/approval/quote_approval_grid",$data);
        }
         function loadRejectedQuotesGrid (){
            $this->load->view("procurement/quote/rejected/quote_rejected_grid",$data);
        }
        
        function loadQuoteFormFragment(){
            $this->load->view("procurement/quote/approval/quote_details",$data);

        }
        function loadQuoteNotesFragment(){
            $this->load->view("procurement/quote/approval/quote_notes",$data);

        }
        
        
        function loadCreateQuotesGrid (){
            $productArray = $this->Product->getValues();
            foreach ($productArray as $product){
                //$data = array('label' => $model['model_name'], 'value' => $model['id']);
                //array_push($autoData,$data);
                $id = $product['id'];
                $name = $product['product_name'].'->'.$product['manufacturer'].' '.$product['model']
                        .' '.$product['measurement_denomination'].' '.$product['uom'];
                $productOptions.="<OPTION VALUE=\"$id\">".$name;
            }
            $data['productOptions'] = $productOptions;
            $this->load->view("procurement/quote/create/quote_create_grid",$data);
        }
        
        function loadReceivedGrid (){
            
            $this->load->view("procurement/purchaseorder/received/received_order_grid",$data);
        }
        
        function loadCreateQuoteFormFragment(){
            $deliveryPoints = $this->Delivery_point->getAll();
            foreach($deliveryPoints as $deliveryPoint) { 

                $id=$deliveryPoint["id"]; 
                $thing=$deliveryPoint["name"]; 
                $options.="<OPTION VALUE=\"$id\">".$thing; 
            } 
            $suppliers= $this->Supplier->getAll();
            foreach($suppliers as $supplier) { 

                $id=$supplier["id"]; 
                $thing=$supplier["supplier_name"]; 
                $supplieroptions.="<OPTION VALUE=\"$id\">".$thing; 
            } 
            $data['warehouseOptions']=$options;
            $data['supplierOptions']=$supplieroptions;
            $this->load->view("procurement/quote/create/quote_details",$data);

        }
        function loadCreateQuoteNotesFragment(){
            $this->load->view("procurement/quote/create/quote_notes",$data);

        }
        function submitForApproval(){
            $idArray = $_REQUEST['ids'];
            if (!empty($idArray)){
                 foreach($idArray as $tempId){
                    $this->db->trans_start();
                    $where_clause_quote = array('id'=>$tempId);
                    $this->Purchase_quote_master->update($where_clause_quote,array('status'=>  self::WAITING_FOR_APPROVAL));
                    log_message('debug',$this->db->last_query());
                    $where_clause_quote_item = array('quote_id'=>$tempId);
                    $this->Purchase_quote_item->update($where_clause_quote_item,array('status'=>self::WAITING_FOR_APPROVAL));
                    log_message('debug',$this->db->last_query());
                    $this->db->trans_complete();
                } 
                
            }
                
        }
        function assign(){
            $inv_id_array  = json_decode($_REQUEST['sel_quote']);
            foreach($inv_id_array as $id){
                $quote_details = $this->Purchase_quote_master->getById($id);
                $notes= $quote_details->notes;
                $notes .= '<br/>Assigned By:' .$this->username.' Assignment Notes:'.$_REQUEST['assignment_notes'];
                $where_clause_array= array('id'=>$id);
                $inv_data = array
                (
                    'owner_id'=>$_REQUEST['userId'],
                    'notes'=>$notes
                    
                );
                $this->Purchase_quote_master->update($where_clause_array,$inv_data);
            }
        }
        
        //receivings 
        function receiveItems(){
            $oper = $_REQUEST["oper"];
            $action = $_REQUEST["action"];
            $order_line_id = $_REQUEST["order_line_id"];
            $order_id = $_REQUEST["order_id"];
            $receiptOp = $_REQUEST["receipt"];
            $receiptIp = $_REQUEST["receipt_ip"];
            $received_quantity  = $_REQUEST["received_quantity"];
            $received_value = $_REQUEST["received_value"];
            $returned_quantity  = $_REQUEST["returned_quantity"];
            $returned_value = $_REQUEST["returned_value"];
            if ($oper == "return"){
                if (!empty($_REQUEST["returned_notes"])){
                    $receipt_item_data['returned_notes']=$_REQUEST["returned_notes"];
                }
            }
            else if ($oper == "receive"){
                if (!empty($_REQUEST["receiving_notes"])){
                    $receipt_item_data['receiving_notes']=$_REQUEST["receiving_notes"];
                }
            }

           
            
            //if (!empty($rcvd_note)){
                $fmatted_rcvd_note = "&#013;&#010; **** ".date("Y-m-d H:i:s", time()). " ****&#013;&#010;".$_REQUEST["receiving_notes"];
                $rcvd_note="CONCAT(`receiving_notes`, '$fmatted_rcvd_note')";
                $notes_array['receiving_notes'] = $rcvd_note;
            //}
            

            //if (!empty($rtrnd_note)){
                $fmatted_rtrnd_note = "&#013;&#010; **** ".date("Y-m-d H:i:s", time()). " ****&#013;&#010;".$_REQUEST["returned_notes"];
                $rtrnd_note = "CONCAT(`returned_notes`, '$fmatted_rtrnd_note')";
               // $receipt_item_data['returned_notes']="CONCAT(`returned_notes`, $fmatted_rtrnd_note);";
                $notes_array['returned_notes'] = $rtrnd_note;
            //}
           
            $product_id = $_REQUEST["product_id"];
            $supplierId=$_REQUEST["supplier_id"];
            $productdetails = $this->Product->getByProductId($product_id,true);
            $receipt_item_data['product_id'] = $product_id;
            $receipt_item_data['name'] = $productdetails['product_name'];
            $receipt_item_data['sku'] = $productdetails['barcode'];
            $receipt_item_data['order_line_id'] = $order_line_id;
            $receipt_item_data['received_quantity']=$received_quantity;
           
            // total received and value for order line items
            $total_received_for_order = $_REQUEST['total_received_quantity'];
            $total_received_value_for_order = $_REQUEST['total_received_value'];
            $total_returned_for_order = $_REQUEST['total_returned_quantity'];
            $total_returned_value_for_order = $_REQUEST['total_returned_value'];
            $ordered_quantity = $_REQUEST['quantity'];
            $order_line_item_matched = 0;
            
            
            $line_status= self::RECEIVING;
            if ($ordered_quantity == $total_received_for_order + $total_returned_for_order){
                $line_status= self::RECEIVED;
                $order_line_item_matched = 1;
            }
            
            
            
            $this->db->trans_start();
            //if new receipt number for this particular suppler create obe entry in recepipt table
            
            if (empty($receiptOp) && !empty($receiptIp)){
                $receiptData = array ('supplier_receipt_number'=>trim($receiptIp),'supplier_id'=>$supplierId
                    ,'order_id'=>$order_id,'status'=>self::RECEIVING,'owner_id'=>$this->user->person_id);
                
                $this->Receipt_master->insert($receiptData);
                log_message('debug','insert statement ='.$this->db->last_query());
                $id = $this->db->insert_id();

                $where_clause = array('id'=>$id);
                $this->Receipt_master->update($where_clause, array('reference' => 10000000 + $id));
                log_message('debug','update statement ='.$this->db->last_query());
                
                $receiptOp = $id;
               
            }
            $receipt_item_data['receipt_id'] = $receiptOp;
            //receipt line items are unique by order line id and receiot id.
            //a single order line can have multiple recepit
            // a single supplier receipt can also have multiple order line and multiple order
            // but system receipt can only have single order and lultiple order line
            $itemDetails = $this->Receipt_item->getByOrderLineAndReceipt($order_line_id,$receiptOp);
            //adding up with already received
            if (!empty($itemDetails) ){
                if ($action == 'add'){
                    // for received goods
                    $savedValue = $itemDetails['received_value']; //this is the receipt line items total value
                    $savedQuantity= $itemDetails['received_quantity'];
                    //update new total
                    $received_value += $savedValue;
                    $received_quantity += $savedQuantity;
                    //for returned goods
                    $savedReturnedValue = $itemDetails['returned_value']; //this is the receipt line items total value
                    $savedReturnedQuantity= $itemDetails['returned_quantity'];
                    //update new total
                    $returned_value += $savedReturnedValue;
                    $returned_quantity += $savedReturnedQuantity;
                }
                
            }
            $receipt_item_data['received_value'] = $received_value;
            $receipt_item_data['received_quantity'] = $received_quantity;
            $receipt_item_data['returned_value'] = $returned_value;
            $receipt_item_data['returned_quantity'] = $returned_quantity;
            $receipt_item_data['status'] = $line_status;
            $receipt_item_data['ordered_quantity'] = $ordered_quantity;
            //save receipt line item
            if (empty($itemDetails)){
                $this->Receipt_item->insert($receipt_item_data);
                log_message('debug','insert statement ='.$this->db->last_query());
                $id = $this->db->insert_id();
                /* end insert  */

                /* update reference number in  quote item */
                $where_clause = array('id'=>$id);
                $this->Receipt_item->update($where_clause, array('reference' => 10000000 + $id));
                
            }
            else {
                $this->Receipt_item->updateWithNotes(array('id'=>$itemDetails['id']),array('received_quantity'=>$received_quantity,'received_value'=>$received_value,'returned_quantity'=>$returned_quantity,
                    'returned_value'=>$returned_value,'status'=>$line_status),$notes_array);
//                $this->Receipt_item->updateWithoutEscape(array('id'=>$itemDetails['id']),'receiving_notes',$rcvd_note);
//                $this->Receipt_item->updateWithoutEscape(array('id'=>$itemDetails['id']),'returned_notes',$rtrnd_note);
                
            }
            //if completed then update all the receipt line items with same order line id to "RECEIVED"
                // as the parent order line id status is soon going to be set as "RECEIVED"
            if ($line_status==self::RECEIVED){
                $this->Receipt_item->update(array('order_line_id'=>$order_line_id),array('status'=>$line_status));
            }
            // update PO line with total value. Note This could be different from the received quantity
            // as there could be multiple receipt line for a single order line
            
            $this->Purchase_order_item->update(array('id'=>$order_line_id),array('received_quantity'=>$total_received_for_order,'received_value'=>$total_received_value_for_order,
                'returned_quantity'=>$total_returned_for_order,'returned_value'=>$total_returned_value_for_order,'status'=>$line_status,'matched'=>$order_line_item_matched));
            //update Purchase Order So That It Can be Ready For Invoicing
            $this->Purchase_order_master->update(array('id'=>$order_id),array('status'=>self::RECEIVING));
            $this->db->trans_complete();
            
        }
//        function receiveItems(){}
        
        function populateReceiptOptions (){
            $order_id= $_REQUEST['order_id'];
            $receipts = $this->Receipt_master->getAll(false,array('order_id'=>$order_id),array('id','supplier_receipt_number'));
            foreach($receipts as $receipt) { 
                $id=$receipt["id"]; 
                $details=$receipt["supplier_receipt_number"]; 
                //$value = $denom["denom_json"];
                if (!empty($id)){
                    $receiptOptions.="<OPTION VALUE=\"$id\">".$details; 
                }             
            } 
            
            echo $receiptOptions;
        }
        
        function createInvoiceUpdateOrderReceipt (){
            $receiptIds = $_REQUEST['receipt_ids'];
            $order_id = $_REQUEST['order_id'];
            $total_invoice_value = 0;
            $this->db->trans_start();
            $invoice_data=array('status'=>self::PENDING,'order_id'=>$order_id,
                    'invoiced_by'=>$this->user->person_id,'owner_id'=>$this->user->person_id);
            $newInvId = $this->createInvoice($invoice_data);
            $order_line_array = array();
            foreach ($receiptIds as $receiptId){
                // create Invoice items
                $receipt_items = $this->Receipt_item->getAll(false,array('receipt_id'=>$receiptId,'status'=>self::RECEIVED),
                        array('sku','name','product_id','price','order_line_id','received_value','received_quantity'));
                
                foreach ($receipt_items as $receipt_item){
                    $item_data['sku'] = $receipt_item['sku'];
                    $item_data['name'] = $receipt_item['name'];
                    $item_data['product_id'] = $receipt_item['product_id'];
                    $item_data['price'] = $receipt_item['price'];
                    $item_data['order_line_id'] = $receipt_item['order_line_id'];
                    $item_data['total_invoiced_value'] = $receipt_item['received_value'];
                    $item_data['invoiced_quantity'] = $receipt_item['received_quantity'];
                    $item_data['invoice_id'] = $newInvId;
                    $this->Purchase_invoice_item->insert($item_data);
                    log_message('debug','insert statement ='.$this->db->last_query());
                    $id = $this->db->insert_id();
                    /* end insert  */

                    /* update reference number in  quote item */
                    $where_clause = array('id'=>$id);
                    $this->Purchase_invoice_item->update($where_clause, array('reference' => 10000000 + $id));
                    // now prepare array to update order line items
                    if (array_key_exists($receipt_item['order_line_id'], $order_line_array)){
                        // already exists .. that means in separate invoice item some of the quantity
                        //already invoiced.. get the old value add with the this invoice line items 
                        // invoiced_quantity. push iit back to array
                        $oldvalue = $order_line_array[$item_data['order_line_id']];
                        $order_line_array[$item_data['order_line_id']] =  $oldvalue + $item_data['invoiced_quantity'];
                    }
                    else {
                        // first invoice item for this order line item
                        $order_line_array[$item_data['order_line_id']] =  $item_data['invoiced_quantity'];
                    }
                }
                //grab order line array .. update order line items 
                foreach ($order_line_array as $key => $value){
                    $this->Purchase_order_item->update(array('id'=>$key),array('invoiced_quantity'=>$value));
                }
                //calcultae total invoice value by receipt id 
                $total_invoice_value +=$this->Receipt_item->totalAmount($receiptId);
                // as of now this is all or none.
                //later add invoiced quantity
                $this->Receipt_master->update(array('id'=>$receiptId,'status'=>self::RECEIVED),array('invoice_id'=>$newInvId,'status'=>self::INVOICED));
                $this->Receipt_item->update(array('receipt_id'=>$receiptId,'status'=>self::RECEIVED),array('status'=>self::INVOICED));
                
            }
            
            $this->Purchase_invoice_master->update(array('id'=>$newInvId),array('total_value'=>$total_invoice_value));
            //validate by order if invoicing complete
            //first find order lines are received. If all order lines are received then there are entries in
            //receipt line item table with all order line items. Next Se if all receipt Items with receipt ids
            //with the order id  are invoiced. If Invoiced then mark all order lines and the order as invoiced
            //(Revisit  This Later)
//            $noOfRows = $this->Receipt_master()->totalNoOfRows(array('order_id'=>$order_id,'status'=>self::RECEIVED));
            $this->db->trans_complete();
            echo $newInvId;
        }
        
        function createInvoice($invoiceData){
            $this->Purchase_invoice_master->insert($invoiceData);
            log_message('debug','insert statement ='.$this->db->last_query());
            $id = $this->db->insert_id();

            $where_clause = array('id'=>$id);
            $this->Purchase_invoice_master->update($where_clause, array('reference' => 10000000 + $id));
            log_message('debug','update statement ='.$this->db->last_query());

           return $id;
               
        }
        
        function registerPayment(){
            
            $total = $_REQUEST['total_value'] ;
            $data['amount'] = $_REQUEST['amount'] ;
            $data['invoice_id'] = $_REQUEST['invoice_id'] ;
            $data['comments'] = $_REQUEST['comments'] ;
            $oper = $_REQUEST['oper'] ;
            $payment_id = $_REQUEST['payment_id'] ;
            $data['payment_reference'] = $_REQUEST['payment_ref'] ;
            $data['payment_type'] = $_REQUEST['payment_type'] ;
            $total_invoiced = $_REQUEST['total_invoiced'];
            
            $this->db->trans_start();
            if ($oper == 'add'){
                $this->Outgoing_payment->insert($data);
            }
            else if (oper == 'edit'){
                $this->Outgoing_payment->update(array('id',$payment_id),$data);
            }
            if ($total_invoiced == $total){
                $this->Purchase_invoice_master->update(array('id'=>$_REQUEST['invoice_id'] ),array('amount_paid'=>$total,'status'=>self::COMPLETE));
            }
            else {
                $this->Purchase_invoice_master->update(array('id'=>$_REQUEST['invoice_id'] ),array('amount_paid'=>$total,'status'=>self::PARTIAL));
            }
            
            $this->db->trans_complete();
            if ($this->db->trans_status()=== TRUE){
                echo $total;
            }
            
        }
        
        function printInvoice(){
            define('FPDF_FONTPATH',$this->config->item('fonts_path'));
            //$this->load->library('fpdf','','fpdf');
            //$this->load->library('table_pdf','','pdf');
//            $this->load->library('rotation');
//            $this->load->library('pdf','','pdf');
            
            
//            $this->pdf->AddPage();
//            $this->pdf->SetFont('Arial','B',16);
//            $this->pdf->Cell(40,10,'Hello World!');
            $header = array('Type', 'Payment Ref', 'Amount');
            $data = $this->Outgoing_payment->getColumnValues(false,null,array('payment_type','payment_reference','amount'));
            $this->pdf->SetFont('Arial','',14);
//            $this->pdf->AddPage();
//            $this->pdf->BasicTable($header,$data);
//            $this->pdf->AddPage();
//            $this->pdf->ImprovedTable($header,$data);
            $this->pdf->AddPage();
            $this->pdf->FancyTable($header,$data);


            //$this->pdf->Output('/tmp/test'.time().'.pdf', 'F');
            $this->pdf->Output('/tmp/test.pdf', 'F');
        }
        
        function generatePdfPurchaseInvoice(){
            
            define('FPDF_FONTPATH',$this->config->item('fonts_path'));
            $this->load->library('pdf_invoice','','pdf');
            $this->pdf->AddPage();
            $invoice_details = $this->Purchase_invoice_master->getById($_REQUEST['invoice_id']);
            $order_details = $this->Purchase_order_master->getGridById($invoice_details->order_id,false);
            $supplier_details = $this->Supplier->getById($order_details->supplier_id);
            $payments = $this->Outgoing_payment->getColumnValues(false,array('invoice_id'=>$invoice_details->id),array('payment_type','payment_reference','amount','comments'));
            $receiptIds = $this->Receipt_master->getColumnValues(false,array('invoice_id'=>$invoice_details->id),array('id'));
            $receiptItems = $this->Receipt_item->getColumnValues(false,array('invoice_id'=>$invoice_details->id),array('id'));
            $payment_height = 20 + count($payments)*2;
            //$supplier_details;
            
            $this->pdf->addSociete("Shopifine Retail LLP",
                              "#440 5th Cross 8th Cross\n" .
                              "Bangalore 560094\n" .
                              "Karnataka\n" 
                              );
            $this->pdf->fact_dev( "Shopifine ", "Invoice" );
            $this->pdf->temporaire( "Shopifine" );
            //$this->pdf->addDate( $invoice_details['created_at']);
            $this->pdf->addClient($supplier_details['supplier_name']);
            $this->pdf->addPageNumber("1");
            $this->pdf->addClientAdresse($supplier_details['address']." ".$supplier_details['city']
                    ." ".$supplier_details['state']);
//            $this->pdf->addReglement("Chèque à réception de facture");
//            $this->pdf->addEcheance("03/12/2003");
            $this->pdf->addNumTVA($supplier_details['registration_number']);
            $contact = $supplier_details['contact_person'];
            //$this->pdf->addReference("$contact");
            $cols=array( "PAYMENT TYPE "    => 35,
                         "PAYMENT REFERENCE"  => 50,
                         "AMOUNT"     => 30,
                         "COMMENTS"      => 70
                         );
            $this->pdf->addCols( $cols,$payment_height);
            $cols=array( "PAYMENT TYPE "    => "C",
                         "PAYMENT REFERENCE"  => "C",
                         "AMOUNT"     => "R",
                         "COMMENTS"      => "L"
                         );
            $this->pdf->addLineFormat( $cols);
            $this->pdf->addLineFormat($cols);
////
            $y    = 109;
            foreach ($payments as $payment){
                $line = $cols=array( "PAYMENT TYPE "    => $payment['payment_type'],
                         "PAYMENT REFERENCE"  => $payment['payment_reference'],
                         "AMOUNT"     => $payment['amount'],
                         "COMMENTS"      => $payment['comments']
                         );
                $size = $this->pdf->addLine( $y, $line );
                $y   += $size + 2;
            }
//
//           
           $this->pdf->addCadreTVAs();
//
            // invoice = array( "px_unit" => value,
            //                  "qte"     => qte,
            //                  "tva"     => code_tva );
            // tab_tva = array( "1"       => 19.6,
            //                  "2"       => 5.5, ... );
            // params  = array( "RemiseGlobale" => [0|1],
            //                      "remise_tva"     => [1|2...],  // {la remise s'applique sur ce code TVA}
            //                      "remise"         => value,     // {montant de la remise}
            //                      "remise_percent" => percent,   // {pourcentage de remise sur ce montant de TVA}
            //                  "FraisPort"     => [0|1],
            //                      "portTTC"        => value,     // montant des frais de ports TTC
            //                                                     // par defaut la TVA = 19.6 %
            //                      "portHT"         => value,     // montant des frais de ports HT
            //                      "portTVA"        => tva_value, // valeur de la TVA a appliquer sur le montant HT
            //                  "AccompteExige" => [0|1],
            //                      "accompte"         => value    // montant de l'acompte (TTC)
            //                      "accompte_percent" => percent  // pourcentage d'acompte (TTC)
            //                  "Remarque" => "texte"              // texte
            $tot_prods = array( array ( "px_unit" => 600, "qte" => 1, "tva" => 1 ),
                                array ( "px_unit" =>  10, "qte" => 1, "tva" => 1 ));
            $tab_tva = array( "1"       => 19.6,
                              "2"       => 5.5);
            $params  = array( "RemiseGlobale" => 1,
                                  "remise_tva"     => 1,       // {la remise s'applique sur ce code TVA}
                                  "remise"         => 0,       // {montant de la remise}
                                  "remise_percent" => 10,      // {pourcentage de remise sur ce montant de TVA}
                              "FraisPort"     => 1,
                                  "portTTC"        => 10,      // montant des frais de ports TTC
                                                               // par defaut la TVA = 19.6 %
                                  "portHT"         => 0,       // montant des frais de ports HT
                                  "portTVA"        => 19.6,    // valeur de la TVA a appliquer sur le montant HT
                              "AccompteExige" => 1,
                                  "accompte"         => 0,     // montant de l'acompte (TTC)
                                  "accompte_percent" => 15,    // pourcentage d'acompte (TTC)
                              "Remarque" => "Avec un acompte, svp..." );

            $this->pdf->addTVAs( $params, $tab_tva, $tot_prods);
            //$this->pdf->addCadreEurosFrancs();
            $this->pdf->Output();
            //$this->pdf->Output('/tmp/test.pdf', 'F');
        }
        
        /* Invoice Grid Start */
        
         function populateInvoicesToPay(){
           $person_id = $this->user->person_id;
           $in_where=array('field_name'=>'status','id_array'=>array(self::RECEIVED)) ;
           $page = $_REQUEST['page'];
           $limit = $_REQUEST['rows'];
           $sidx = $_REQUEST['sidx'];
           $sord = $_REQUEST['sord'];
           $searchOn = $_REQUEST['_search'];
           
           if (!empty($person_id)){
                $where['owner_id'] = $person_id;
           }
           
           //standard response parameters 
           $quotedata = array();
           $count = $this->Purchase_invoice_master->totalNoOfRows($where);
           if( $count > 0 && $limit > 0) { 
               $total_pages = ceil($count/$limit); 
           } else { 
               $total_pages = 0; 
           } 
           if ($page > $total_pages) $page=$total_pages;

           $start = $limit*$page - $limit;
 
            // if for some reasons start position is negative set it to 0 
            // typical case is that the user type 0 for the requested page 
           if($start <0) $start = 0; 
           $clauses = array('orderBy'=>$sidx,'orderDir'=>$sord,'startLimit'=>$start,'limit'=>$limit);
            
           $data['total'] = $total_pages;
           $data['page'] = $page;
           $data['records'] = $count; 
           if($searchOn=='true') {
                
                $filters = json_decode($_REQUEST['filters'],true);
                $groupOp = $filters['groupOp'];
                $rules = $filters['rules'];
                $like_condition = array();
                foreach ($rules as $rule){
                    $field = $rule['field'];
                    $op= $rule['op'];
                    $input = $rule['data'];
                    
                    $like_condition[$field] = trim($input);
                   
                }
                $quotes = $this->Purchase_invoice_master->getAll(false,$where,$clauses,$like_condition);
            }
            else {
                $quotes = $this->Purchase_invoice_master->getAll(false,$where);
            }
           
           foreach ($quotes as $dp){
               $orderDetails = $this->Purchase_order_master->getGridById($dp['order_id']);
               $userDetails = $this->User->getUserInfo($dp['invoiced_by'],true);
               array_push($quotedata, array('id'=> $dp['id'],'dprow' => array($dp['reference'],$dp['total_value'],$dp['amount_paid'],$dp['status'],$userDetails['username'],$dp['order_id'],$orderDetails['reference'])));
           }
           $data['quotedata'] = $quotedata;
           echo json_encode($data);
        }
        
        function populateInvoiceItems(){
            $invoiceId = $_REQUEST['invoiceId'];
            if (!empty($invoiceId)){
                $page = $_REQUEST['page'];
                $limit = $_REQUEST['rows'];
                $sidx = $_REQUEST['sidx'];
                $sord = $_REQUEST['sord'];

                //standard response parameters 

                $quotedata = array();
                $where = array('invoice_id' => $invoiceId );
                $count = $this->Purchase_invoice_item->totalNoOfRows($where);
                if( $count > 0 && $limit > 0) { 
                    $total_pages = ceil($count/$limit); 
                } else { 
                    $total_pages = 0; 
                } 
                if ($page > $total_pages) $page=$total_pages;

                $start = $limit*$page - $limit;

                 // if for some reasons start position is negative set it to 0 
                 // typical case is that the user type 0 for the requested page 
                if($start <0) $start = 0; 
                $clauses = array('orderBy'=>$sidx,'orderDir'=>$sord,'startLimit'=>$start,'limit'=>$limit);

                $data['total'] = $total_pages;
                $data['page'] = $page;
                $data['records'] = $count; 

                $quotes = $this->Purchase_invoice_item->getAll(false,$where);
                

                foreach ($quotes as $dp){
                    //$order_line = $this->Purchase_order_item->getById($dp['order_line_id']);
                    
                    array_push($quotedata, array('id'=> $dp['id'],'dprow' => array($dp['product_id'],$dp['name'],$dp['invoiced_quantity'],$dp['total_invoiced_value'])));
                }
                $data['invoiceitemdata'] = $quotedata;
                echo json_encode($data);
           }         
           
        }
        
}
?>