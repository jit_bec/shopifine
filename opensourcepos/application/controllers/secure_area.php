<?php
class Secure_area extends CI_Controller 
{
	
        const ORDERED = 'ordered';
        const CANCELLED = 'cancelled';
        const OPEN = 'open';
        const PENDING = 'pending';
        const COMPLETE = 'complete';
        const PARTIAL = 'partiallypaid';
        const WAITING_FOR_APPROVAL = 'waitingforapproval';
        const RECEIVING = 'receiving';
        const RECEIVED = 'received';
        const REJECTED = 'rejected';
        const INVOICING = 'invoicing';
        const INVOICED = 'invoiced';
        const ALL = 'all';
        /*
	Controllers that are considered secure extend Secure_area, optionally a $module_id can
	be set to also check if a user can access a particular module in the system.
	*/
	function __construct($module_id=null,$type='menu')
	{
		parent::__construct();	
                $this->load->library('acl-library/Acl','','Acl');
		$this->load->model('acl/User','User');
                $this->load->model('acl/Role','Role');
                $this->load->model('acl/Permission','Permission');
		if(!$this->User->is_logged_in())
		{
			redirect('login');
		}
                $username = $this->User->get_logged_in_employee_info()->username;
		
		if(!$this->Acl->isUserAllowed($module_id.'-'.$type,'all',$username))
		{
			redirect('no_access/'.$module_id);
		}
		
		//load up global data
//                $CI =& get_instance();
//$CI->magento = $this->load->database('magento', TRUE);
//$this->magento =$CI->magento; 
                $CI =& get_instance();
                $CI->magento = $CI->load->database('magento', TRUE);
		$logged_in_employee_info=$this->User->get_logged_in_employee_info();
                $html = $this->buildMenu($logged_in_employee_info);
                $adminhtml = $this->buildAdminMenu($logged_in_employee_info);
                $data['menu'] = $html;
                $data['adminmenu'] = $adminhtml;
		$data['allowed_modules']=$this->Module->get_allowed_modules($logged_in_employee_info->person_id);
		$data['user_info']=$logged_in_employee_info;
		$this->load->vars($data);
	}
        
//        function __construct($module_id=null)
//	{
//		parent::__construct();	
//		$this->load->model('Employee');
//		if(!$this->Employee->is_logged_in())
//		{
//			redirect('login');
//		}
//		
//		if(!$this->Employee->has_permission($module_id,$this->Employee->get_logged_in_employee_info()->person_id))
//		{
//			redirect('no_access/'.$module_id);
//		}
//		
//		//load up global data
////                $CI =& get_instance();
////$CI->magento = $this->load->database('magento', TRUE);
////$this->magento =$CI->magento; 
//                $CI =& get_instance();
//                $CI->magento = $CI->load->database('magento', TRUE);
//		$logged_in_employee_info=$this->Employee->get_logged_in_employee_info();
//                $html = $this->buildMenu($logged_in_employee_info);
//                $data['menu'] = $html;
//		$data['allowed_modules']=$this->Module->get_allowed_modules($logged_in_employee_info->person_id);
//		$data['user_info']=$logged_in_employee_info;
//		$this->load->vars($data);
//	}
        
//        private function buildMenu ($logged_in_employee_info){
//            $allowedModules = $this->Permission->getMenu($logged_in_employee_info->username);
//            $html;
//            $style = addslashes("border-top-left-radius: 5px;
//    border-top-right-radius: 5px;");
//            foreach($allowedModules as $module)
//            {
//                $html .= "<li><a style='".$style."' class="."fNiv"." href=".site_url($module->relative_path_link).">".$module->ui_display_name."</a>";
//                $allowedSubMenu =  $this->Permission->getSubMenu($logged_in_employee_info->username,$module->id);
//                if (!empty($allowedSubMenu)){
//                    $html.= "<ul>";
//                }
//                
//                foreach($allowedSubMenu as $submenu){
//                    $html .= "<li>
//                                    <a href=".site_url($submenu->relative_path_link). ">".$submenu->ui_display_name."
//                                        </a>
//                                </li>";                 
//                }
//                if (!empty($allowedSubMenu)){
//                    $html.= "</ul>";
//                }
//                $html .= "</li>";
//            }
//               	
//            return $html;
//        }
        
        private function buildMenu ($logged_in_employee_info){
            $allowedModules = $this->Permission->getMenu($logged_in_employee_info->username);
            $html;
            $finalpermissions = array();
            
            foreach($allowedModules as $module){
                $resource= $module['resource'];
                $permission=$module['isAllowed'];
                $finalpermissions[$resource] = $permission;
            }
            
            foreach ($allowedModules as $key => $row) {
                $order[] =$row['relative_order_in_category'] ; 
                // of course, replace 0 with whatever is the date field's index
            }
          
//
            array_multisort($order, SORT_ASC, $allowedModules);
            
            $style = addslashes("border-top-left-radius: 5px;
    border-top-right-radius: 5px;");
            foreach($allowedModules as $module)
            {
                if ($finalpermissions[$module['resource']]==1){
                    $html .= "<li><a style='".$style."' class="."fNiv"." href=".site_url($module['relative_path_link']).">".$module['ui_display_name']."</a>";
                    $allowedSubMenu =  $this->Permission->getSubMenu($logged_in_employee_info->username,$module['id']);
                    $finalsubmenupermissions = array();
                    foreach($allowedSubMenu as $submenu){
                        $subMenuResource= $submenu['resource'];
                        $subMenuPermission=$submenu['isAllowed'];
                        $finalsubmenupermissions[$subMenuResource] = $subMenuPermission;
                    }
                    //checking if there is any submenu at all
                        if (!empty($finalsubmenupermissions)){
                            $html.= "<ul>";
                        }
                        
                        foreach ($allowedSubMenu as $key => $row) {
                            $orderSM[] =$row['relative_order_in_category'] ; 
                            // of course, replace 0 with whatever is the date field's index
                        }
                        
                        if (!empty($orderSM)){
                            array_multisort($orderSM, SORT_ASC, $allowedSubMenu);
                            unset($orderSM);
                        }
            //
                        

                        foreach($allowedSubMenu as $submenu){
                            if ($finalsubmenupermissions[$submenu['resource']]==1){
                                $html .= "<li>
                                                <a href=".site_url($submenu['relative_path_link']). ">".$submenu['ui_display_name']."
                                                    </a>
                                            </li>";    
                                $finalsubmenupermissions[$submenu['resource']]==0;
                            }
                        }
                        if (!empty($allowedSubMenu)){
                            $html.= "</ul>";
                        }
                    $html .= "</li>";
                    $finalpermissions[$module['resource']]=0;
                }
            }
               	
            return $html;
        }
        
        private function buildAdminMenu ($logged_in_employee_info){
            
            $allowedModules = $this->Permission->getAdminMenu($logged_in_employee_info->username);
            $html;
            $finalpermissions = array();
            
            foreach($allowedModules as $module){
                $resource= $module['resource'];
                $permission=$module['isAllowed'];
                $finalpermissions[$resource] = $permission;
            }
            
            foreach ($allowedModules as $key => $row) {
                $order[] =$row['relative_order_in_category'] ; 
                // of course, replace 0 with whatever is the date field's index
            }
//
            array_multisort($order, SORT_ASC, $allowedModules);
            
            $style = addslashes("border-top-left-radius: 5px;
    border-top-right-radius: 5px;");
            foreach($allowedModules as $module)
            {
                if ($finalpermissions[$module['resource']]==1){
                    $html .= "<li><a style='".$style."' class="."fNiv"." href=".site_url($module['relative_path_link']).">".$module['ui_display_name']."</a>";
                    $allowedSubMenu =  $this->Permission->getSubMenu($logged_in_employee_info->username,$module['id']);
                    $finalsubmenupermissions = array();
                    foreach($allowedSubMenu as $submenu){
                        $subMenuResource= $submenu['resource'];
                        $subMenuPermission=$submenu['isAllowed'];
                        $finalsubmenupermissions[$subMenuResource] = $subMenuPermission;
                    }
                    //checking if there is any submenu at all
                        if (!empty($finalsubmenupermissions)){
                            $html.= "<ul>";
                        }
                        foreach ($allowedSubMenu as $key => $row) {
                            $orderSM[] =$row['relative_order_in_category'] ; 
                            // of course, replace 0 with whatever is the date field's index
                        }
            //
                        if (!empty($orderSM)){
                            array_multisort($orderSM, SORT_ASC, $allowedSubMenu);
                            unset($orderSM);
                        }

                        foreach($allowedSubMenu as $submenu){
                            if ($finalsubmenupermissions[$submenu['resource']]==1){
                                $html .= "<li>
                                                <a href=".site_url($submenu['relative_path_link']). ">".$submenu['ui_display_name']."
                                                    </a>
                                            </li>";    
                            }
                        }
                        if (!empty($allowedSubMenu)){
                            $html.= "</ul>";
                        }
                    $html .= "</li>";
                }
            }
               	
            return $html;
            
//            $allowedModules = $this->Permission->getAdminMenu($logged_in_employee_info->username);
//            $html;
//            foreach($allowedModules as $module){
//                $resource= $module->resource;
//                $permission=$module->isAllowed;
//                $finalpermissions[$resource] = $permission;
//            }
//            
//            $style = addslashes("border-top-left-radius: 5px;
//    border-top-right-radius: 5px;");
//            foreach($allowedModules as $module)
//            {
//                if ($finalpermissions[$module->resource]==1){
//                    $html .= "<li><a style='".$style."' class="."fNiv"." href=".site_url($module->relative_path_link).">".$module->ui_display_name."</a>";
//                    $allowedSubMenu =  $this->Permission->getSubMenu($logged_in_employee_info->username,$module->id);
//                    foreach($allowedSubMenu as $submenu){
//                        $subMenuResource= $submenu->resource;
//                        $subMenuPermission=$submenu->isAllowed;
//                        $finalsubmenupermissions[$subMenuResource] = $subMenuPermission;
//                    }
//                    //checking if there is any submenu at all
//                        if (!empty($finalsubmenupermissions)){
//                            $html.= "<ul>";
//                        }
//                        
//
//                        foreach($allowedSubMenu as $submenu){
//                            if ($finalsubmenupermissions[$submenu->resource]==1){
//                                $html .= "<li>
//                                                <a href=".site_url($submenu->relative_path_link). ">".$submenu->ui_display_name."
//                                                    </a>
//                                            </li>";    
//                            }
//                        }
//                        if (!empty($allowedSubMenu)){
//                            $html.= "</ul>";
//                        }
//                    $html .= "</li>";
//                }
//            }
//               	
//            return $html;
        }
        
//         private function buildAdminMenu ($logged_in_employee_info){
//            $allowedModules = $this->Permission->getAdminMenu($logged_in_employee_info->username);
//            $html;
//            $style = addslashes("border-top-left-radius: 5px;
//    border-top-right-radius: 5px;");
//            foreach($allowedModules as $module)
//            {
//                $html .= "<li><a style='".$style."' class="."settings-icon"." href=".site_url($module->relative_path_link).">&nbsp;&nbsp;&nbsp;&nbsp;".$module->ui_display_name."</a>";
//                $allowedSubMenu =  $this->Permission->getSubMenu($logged_in_employee_info->username,$module->id);
//                if (!empty($allowedSubMenu)){
//                    $html.= "<ul>";
//                }
//                
//                foreach($allowedSubMenu as $submenu){
//                    $html .= "<li>
//                                    <a href=".site_url($submenu->relative_path_link). ">".$submenu->ui_display_name."</a>
//                                </li>";                 
//                }
//                if (!empty($allowedSubMenu)){
//                    $html.= "</ul>";
//                }
//                $html .= "</li>";
//            }
//               	
//            return $html;
//        }
}
?>