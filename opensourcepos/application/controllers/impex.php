<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 *
 * @author abhijit
 */
require_once ("secure_area.php");
class Impex extends Secure_area {
    function __construct()
	{
		parent::__construct('impex','adminmenu');
               
	}
	
	function index(){
                 
               
		
	}
        
        public function importAttributeSets (){
            $status = $this->Attribute_set->importAttributeSets();
            log_message('debug', 'status of Attribute set import '.$status);
        }
        
        public function importCategories(){
           $status =  $this->Category->importCategories();
           log_message('debug', 'status of category import '.$status);
        }
        
        public function importInvoices(){
           $status ="invoiced";
                
            $this->db->trans_start();
            $magentoInvoices = $this->Invoice_master->getUnprocessedInvoicesFromMagento();

            //first make an entry in invoice master
            //if (!$this->Invoice_master->exists($invoiceId)) { //insert

            foreach ($magentoInvoices as $inv){
                $inv_entity_id = $inv['entity_id'];
                $inv_increment_id = $inv['invoice_increment_id'];
                $order_id = $inv['order_id'];
                $order_increment_id = $inv['order_increment_id'];
                $inv_data = array
                (
                        'magento_invoice_increment_id'=>$inv_increment_id,
                        'magento_invoice_entity_id'=>$inv_entity_id,
                        'magento_order_increment_id'=>$order_increment_id,
                        'magento_order_id'=>$order_id,
                        'status'=>$status,
                        'created_at'=>date('Y-m-d H:i:s') 

                );

                $this->Invoice_master->insert($inv_data);

            }

            $items = $this->Invoice_item->getUnprocessedInvoiceItemsFromMagento();
            foreach ($items as $item){
                $magento_entity_id = $item['entity_id'];
                $inv_entity_id = $item['parent_id'];
                $inv_increment_id = $item['increment_id'];
                $product_id = $item['product_id'];
                $type = $item['type_id'];
                $sku= $item['sku'];
                $name = $item['name'];
                $qty = $item['qty'];
                $inv_item_data = array
                    (
                            'magento_entity_id'=>$magento_entity_id,
                            'magento_invoice_entity_id'=>$inv_entity_id,
                            'invoice_id' =>$inv_increment_id,
                            'sku'=>$sku,
                            'name'=>$name,
                            'type'=>$type,
                            'magento_product_id'=>$product_id,
                            'invoiced_number'=>$qty,
                            'created_at'=>date('Y-m-d H:i:s'),

                    );
                $this->Invoice_item->insert($inv_item_data);

            }


            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                //echo $this->db->_error_message();
                //die( 'Transaction Failed while inserting invoice records. Please check log');
            }
        }
        
        function exportProductsMagmi (){
            $error = false;
            $result = $this->Product->getMagentoExportView();
            $resource = $this->Resource->getByResourceName('exportdir-impexconfig');
            
            $filename = $resource['relative_path_link'] . $this->Appconfig->get('create_product_file');
//            $filename = $result['relative_path_link'];
            $this->load->helper('file');
            
            //var_dump(($result));
            //delete_files($filename);
            //delete_files($filename);
             if ( ! write_file($filename,$result ,'w+'))
            {
               $error = true;
            }
            $data['mode'] = $this->Appconfig->get('create_product_mode');
            $data['profile'] = $this->Appconfig->get('create_product_profile');
            $data['filename'] = $filename;
            $data['error'] = $error;
            $this->load->view('impex/process_magento',$data);
            
        }
        
        function dbprefix (){
            var_dump ($this->db->dbprefix);
        }
    }
    
    

?>
