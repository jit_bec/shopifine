<?php

class Day_Two_RenderController
        extends Mage_Core_Controller_Front_Action {
    
    public function indexAction(){
        echo 'In render controller';
    }
    
    public function layoutAction(){
       //$this->loadLayout();
       $this->loadLayout()->generateLayoutXml();
       $this->renderLayout();
       var_dump($this->getLayout()->getUpdate()->getHandles());
       //var_dump($this->getLayout()->getUpdate()->fetchFileLayoutUpdates());
    }
}