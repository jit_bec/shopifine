<?php

class Day_Two_IndexController extends Mage_Core_Controller_Front_Action{
    
    public function indexAction(){
        echo 'Yeppi';
    }
    
    public function layoutAction(){
        
        $xml = $this->loadLayout()->getLayout()->getUpdate()->asString();
        
        $this->getResponse()->setHeader('Content-Type', 'text/plain')->setBody($xml);
    }
}