<?php
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->run("

INSERT INTO `{$this->getTable('area_covered')}` VALUES (5,5,'Banerghatta Road'),(6,6,'Sarjapur Road'),(7,7,'HSR Layout'),(8,8,'Madiwala');

");

$installer->endSetup();