<?php

$installer = $this;

$installer->startSetup();

$installer->run("

CREATE TABLE IF NOT EXISTS {$this->getTable('area_covered')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `area_id` int(11) unsigned NOT NULL,
  `area_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `{$this->getTable('area_covered')}` VALUES (1,1,'Koramangala'),(2,2,'BTM Layout 1st Stage'),(3,3,'BTM Layout 2nd Stage'),(4,4,'Indiranagar');

");



$installer->endSetup(); 