<?php

class Excellence_Area_Model_Mysql4_Coverage_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('area/coverage');
    }
}

