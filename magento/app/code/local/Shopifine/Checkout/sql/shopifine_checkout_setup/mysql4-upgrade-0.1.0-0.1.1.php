<?php

$installer = $this;

$installer->startSetup();

$installer->run("

ALTER TABLE sales_quote_shopifine DROP COLUMN delivery_date;
ALTER TABLE sales_quote_shopifine ADD COLUMN delivery_date DATE;        

ALTER TABLE sales_order_shopifine DROP COLUMN delivery_date;
ALTER TABLE sales_order_shopifine ADD COLUMN delivery_date DATE; 


    ");

$installer->endSetup(); 