<?php

$installer = $this;

$installer->startSetup();

$installer->run("

CREATE TABLE IF NOT EXISTS {$this->getTable('sales_quote_shopifine')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `quote_id` int(11) unsigned NOT NULL,
  `delivery_slot` varchar(50) ,
  `delivery_date` varchar(50) ,
  `area` varchar(50),
  `alternative_drop` varchar(50),
  `alt_delivery_date` varchar(50) ,
   `mobile_number` varchar(20) ,
  `product_suggestion` varchar(255) ,
  `comments` text ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS {$this->getTable('sales_order_shopifine')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `order_id` int(11) unsigned NOT NULL,
  `delivery_slot` varchar(50) ,
  `delivery_date` varchar(50) ,
  `area` varchar(50),
  `alternative_drop` varchar(50),
  `alt_delivery_date` varchar(50) ,
   `mobile_number` varchar(20) ,
  `product_suggestion` varchar(255) ,
  `comments` text ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 