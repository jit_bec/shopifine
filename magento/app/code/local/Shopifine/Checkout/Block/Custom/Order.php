<?php

class Shopifine_Checkout_Block_Custom_Order extends Mage_Core_Block_Template {
    
    public function getDeliveryInfo(){
        $model = Mage::getModel('shopifine_checkout/custom_order');
        return $model->getByOrder($this->getOrder()->getId());
    }
    public function getOrder()
    {
        return Mage::registry('current_order');
    }
    
}
