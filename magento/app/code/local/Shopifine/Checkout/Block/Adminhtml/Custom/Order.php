<?php

class Shopifine_Checkout_Block_Adminhtml_Custom_Order extends Mage_Adminhtml_Block_Sales_Order_Abstract{
	public function getDeliveryInfo(){
		$model = Mage::getModel('shopifine_checkout/custom_order');
		return $model->getByOrder($this->getOrder()->getId());
	}
}
