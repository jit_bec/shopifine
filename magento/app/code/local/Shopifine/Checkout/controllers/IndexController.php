<?php

class Shopifine_Checkout_IndexController extends Mage_Core_Controller_Front_Action{
    
    public function testAction(){
        echo 'in shopifine';
        Mage::dispatchEvent('sales_model_service_quote_submit_after');
       
    }
    
     protected function _addOrderId(Mage_Sales_Model_Resource_Order_Invoice_Item_Collection $collection)
    {
        $collection->getSelect()->join(
            array('invoice_flat' => $collection->getTable('invoice')),
            'main_table.parent_id = invoice_flat.entity_id',
            array(
                'order_id' => 'invoice_flat.order_id'
            )
        );
            var_dump((string)$collection->getSelect());
        return $this;
    }
    public function test1Action(){
        $invoiceId = 5;
        $collection = Mage::getResourceModel('sales/order_invoice_item_collection');
        $collection->addFieldToFilter('parent_id', $invoiceId);
        $this->_addOrderId($collection);
        
        //$this->_applyCollectionModifiers($collection);
    }
    
    public function quoteModelAction(){
        $quote = Mage::getModel('shopifine_checkout/custom_quote');
        echo 'Model == '.get_class($quote).'<br/>';
        
        $quoteRM = $quote->getResource();
        echo 'ResourceModel == '.get_class($quoteRM).'<br/>';
        
        $quoteCol = $quote->getCollection();
        echo 'Collection == '.get_class($quoteCol).'<br/>';
        
    }
    
     public function orderModelAction(){
       
        $order = Mage::getModel('shopifine_checkout/custom_order');
        echo 'Model == '.get_class($order).'<br/>';
        
        $orderRM = $order->getResource();
        echo 'ResourceModel == '.get_class($orderRM).'<br/>';
        
        $orderCol = $order->getCollection();
        echo 'Collection == '.get_class($orderCol).'<br/>';
    }
    
    
    public function observerAction(){
        Mage::dispatchEvent('sales_quote_save_before');
        // Mage::dispatchEvent('sales_quote_save_after');
        Mage::dispatchEvent('sales_order_load_after');
        Mage::dispatchEvent('sales_model_service_quote_submit_after');
       
//        Mage::dispatchEvent('sales_quote_load_after');
        
    }
    
     public function observerQuoteAction(){
         Mage::dispatchEvent('sales_quote_save_after');
        
     }
     
     

}