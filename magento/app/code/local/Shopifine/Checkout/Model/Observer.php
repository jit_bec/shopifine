<?php
class Shopifine_Checkout_Model_Observer{
    
	public function saveQuoteBefore($evt){

            $quote = $evt->getQuote();
		$post = Mage::app()->getFrontController()->getRequest()->getPost();
		if(isset($post['custom']['delivery_date'])){
			$var = $post['custom']['delivery_date'];
			$quote->setDeliveryDate($var);
		}
                if(isset($post['custom']['delivery_slot'])){
			$var = $post['custom']['delivery_slot'];
			$quote->setDeliverySlot($var);
		}
                if(isset($post['custom']['comments'])){
			$var = $post['custom']['comments'];
			$quote->setComments($var);
		}
       
        }
        
        public function saveQuoteAfter($evt){
 
             $quote = $evt->getQuote();
		if($quote->getId() ){
                        $var = $quote->getId() ;
			
			if(!empty($var)){
                                $deliveryDate= $quote->getDeliveryDate();
                                $deliverySlot= $quote->getDeliverySlot();
                                $comments= $quote->getComments();
				$model = Mage::getModel('shopifine_checkout/custom_quote');
				//$model->deleteByQuote($quote->getId());
                                $id= $model->getIdByQuoteId($var);
                                if(!empty($id)){
                                    $model->load($id);
                                }
				$model->setQuoteId($quote->getId());
                                if(!empty($deliveryDate)){
                                    $model->setDeliveryDate($deliveryDate);
                                }
				if(!empty($deliverySlot)){
                                    $model->setDeliverySlot($deliverySlot);
                                }
                                if(!empty($comments)){
                                    $model->setComments($comments);
                                }
                                
				$model->save();
 			}
		}
             
        }
	
        public function loadQuoteAfter($evt){
 
                 $quote = $evt->getQuote();
		$model = Mage::getModel('shopifine_checkout/custom_quote');
		$data = $model->getByQuote($quote->getId());
		foreach($data as $key => $value){
			$quote->setData($key,$value);
		}
        }
	public function saveOrderAfter($evt){

             $order = $evt->getOrder();
		$quote = $evt->getQuote();
		if($quote->getId()){
                        
                        $orderId = $order->getId();
			if(!empty($orderId)){
                                
                                $deliveryDate= $quote->getDeliveryDate();
                                $deliverySlot= $quote->getDeliverySlot();
                                $comments= $quote->getComments();
				$model = Mage::getModel('shopifine_checkout/custom_order');
				//$model->deleteByOrder($order->getId());
				$model->setOrderId($orderId);
                                $id= $model->getIdByOrderId($orderId);
                                if(!empty($id)){
                                    $model->load($id);
                                }
				if(!empty($deliveryDate)){
                                    $model->setDeliveryDate($deliveryDate);
                                }
				if(!empty($deliverySlot)){
                                    $model->setDeliverySlot($deliverySlot);
                                }
                                if(!empty($comments)){
                                    $model->setComments($comments);
                                }
				$order->setDeliveryDate($deliveryDate);
                                $order->setDeliverySlot($deliverySlot);
                                $order->setComments($comments);
				$model->save();
                                //Mage::dispatchEvent('sales_order_resource_init_virtual_grid_columns');
                                /**
                                * Get the resource model
                                */
                                $resource = Mage::getSingleton('core/resource');

                                /**
                                * Retrieve the write connection
                                */
                                $writeConnection = $resource->getConnection('core_write');
                                $query = "update sales_flat_order_grid set  delivery_date= '".$deliveryDate."',delivery_slot= '".$deliverySlot."',comments= '".$comments."' where entity_id= "
                                .(int) $orderId;
                                $writeConnection->query($query);
			}
		}
        }
        
        
	
	public function loadOrderAfter($evt){

		$order = $evt->getOrder();
		$model = Mage::getModel('shopifine_checkout/custom_order');
		$data = $model->getByOrder($order->getId());
		foreach($data as $key => $value){
			$order->setData($key,$value);
		}
	}

}