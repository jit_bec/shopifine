<?php

class Shopifine_Checkout_Model_Mysql4_Custom_Quote extends Mage_Core_Model_Mysql4_Abstract{
	
    public function _construct()
    {
            $this->_init('shopifine_checkout/custom_quote', 'id');
    }
    
    public function deleteByQuote($quote_id){
		$table = $this->getMainTable();
		$where = $this->_getWriteAdapter()->quoteInto('quote_id = ?', $quote_id);
		//.$this->_getWriteAdapter()->quoteInto('`delivery_slot` = ? 	', $var);
		$this->_getWriteAdapter()->delete($table,$where);
	}
	public function getByQuote($quote_id){
		$table = $this->getMainTable();
		$where = $this->_getReadAdapter()->quoteInto('quote_id = ?', $quote_id);
//		if(!empty($var)){
//			$where .= $this->_getReadAdapter()->quoteInto(' AND `delivery_slot` = ? ', $var);
//		}
		$sql = $this->_getReadAdapter()->select()->from($table)->where($where);
		$row = $this->_getReadAdapter()->fetchRow($sql);
                
                
		$return = array();
		//foreach($rows as $row){
                if($row['quote_id']){
                    $return['quote_id'] = $row['quote_id'];
                }
                if($row['delivery_slot']){
                    $return['delivery_slot'] = $row['delivery_slot'];
                }
                if($row['delivery_date']){
                    $return['delivery_date'] = $row['delivery_date'];
                }
                if($row['comments']){
                    $return['comments'] = $row['comments'];
                }
			
                        
		//}
		return $return;
	}
        
         public function getIdByQuoteId($quote_id){
            if (!empty($quote_id)){
                $table = $this->getMainTable();
                $where = $this->_getReadAdapter()->quoteInto('quote_id = ?', $quote_id);
                $sql = $this->_getReadAdapter()->select()->from($table,array('id'))->where($where);
                $row = $this->_getReadAdapter()->fetchRow($sql);

                return $row['id'];
            }
            return null;
        }
}
