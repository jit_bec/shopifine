<?php

class Shopifine_Checkout_Model_Mysql4_Custom_Order extends Mage_Core_Model_Mysql4_Abstract{
	public function _construct()
	{
		$this->_init('shopifine_checkout/custom_order', 'id');
	}
        
        public function deteleByOrder($order_id){
		$table = $this->getMainTable();
		$where = $this->_getWriteAdapter()->quoteInto('order_id = ? ', $order_id);
		//.$this->_getWriteAdapter()->quoteInto('`key` = ? 	', $var);
		$this->_getWriteAdapter()->delete($table,$where);
	}
        
	public function getByOrder($order_id){
		$table = $this->getMainTable();
		$where = $this->_getReadAdapter()->quoteInto('order_id = ?', $order_id);
//		if(!empty($var)){
//			$where .= $this->_getReadAdapter()->quoteInto(' AND `key` = ? ', $var);
//		}
		$sql = $this->_getReadAdapter()->select()->from($table)->where($where);
		$row = $this->_getReadAdapter()->fetchRow($sql);
		$return = array();
		//foreach($rows as $row){
                if($row['order_id']){
                    $return['order_id'] = $row['order_id'];
                }
		if($row['delivery_slot']){
                    $return['delivery_slot'] = $row['delivery_slot'];
                }
                if($row['delivery_date']){
                    $return['delivery_date'] = $row['delivery_date'];
                }
                if($row['comments']){
                    $return['comments'] = $row['comments'];
                }
		//}
		return $return;
	}
        
        public function getIdByOrderId($order_id){
            if (!empty($order_id)){
                $table = $this->getMainTable();
                $where = $this->_getReadAdapter()->quoteInto('order_id = ?', $order_id);
                $sql = $this->_getReadAdapter()->select()->from($table,array('id'))->where($where);
                $row = $this->_getReadAdapter()->fetchRow($sql);

                return $row['id'];
            }
            return null;
        }
        
       
}
