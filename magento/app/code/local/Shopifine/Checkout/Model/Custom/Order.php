<?php

class Shopifine_Checkout_Model_Custom_Order extends Mage_Core_Model_Abstract {
    
    public function _construct() {
        parent::_construct();
        $this->_init('shopifine_checkout/custom_order');
    }
    
    public function deleteByOrder($order_id){
		$this->_getResource()->deleteByOrder($order_id);
    }
    public function getByOrder($order_id){
            return $this->_getResource()->getByOrder($order_id);
    }
    
    public function getIdByOrderId($order_id){
        return $this->_getResource()->getIdByOrderId($order_id);
    }
}
