<?php
/**
 * Setup scripts, add new column and fulfills
 * its values to existing rows
 *
 */
/* @var $this Mage_Sales_Model_Mysql4_Setup */
$this->startSetup();
$select = $this->getConnection()->select();
$select->join(
    array('shopifine_order'=>$this->getTable('sales_order_shopifine')),    
        'shopifine_order.order_id = order_grid.entity_id ',
    array('delivery_slot' => 'delivery_slot')
);
$this->getConnection()->query(
    $select->crossUpdateFromSelect(
        array('order_grid' => $this->getTable('sales_flat_order_grid'))
    )
);
$this->endSetup();