<?php
/**
 * Setup scripts, add new column and fulfills
 * its values to existing rows
 *
 */
/* @var $this Mage_Sales_Model_Mysql4_Setup */
$this->startSetup();
// Add column to grid table
$this->getConnection()->addColumn(
    $this->getTable('sales_flat_order_grid'),
    'delivery_slot',
    "varchar(50) not null default ''"
);
// Add key to table for this field,
// it will improve the speed of searching & sorting by the field
$this->getConnection()->addKey(
    $this->getTable('sales_flat_order_grid'),
    'delivery_slot',
    'delivery_slot'
);
// Now you need to fullfill existing rows with data from address table
$select = $this->getConnection()->select();
$select->join(
    array('shopifine_order'=>$this->getTable('sales_order_shopifine')),
    
        'shopifine_order.order_id = order_grid.entity_id '
    ,
    array('delivery_slot' => 'delivery_slot')
);
$this->getConnection()->query(
    $select->crossUpdateFromSelect(
        array('order_grid' => $this->getTable('sales_flat_order_grid'))
    )
);
$this->endSetup();