<?php
/**
 * Setup scripts, add new column and fulfills
 * its values to existing rows
 *
 */
/* @var $this Mage_Sales_Model_Mysql4_Setup */
$this->startSetup();
// Add column to grid table
$this->getConnection()->addColumn(
    $this->getTable('sales/order_grid'),
    'delivery_date',
    "date "
);
$this->getConnection()->addColumn(
    $this->getTable('sales/order_grid'),
    'comments',
    "varchar(255) "
);
// Add key to table for this field,
// it will improve the speed of searching & sorting by the field
$this->getConnection()->addKey(
    $this->getTable('sales/order_grid'),
    'delivery_date',
    'delivery_date'
);

$this->getConnection()->addKey(
    $this->getTable('sales/order_grid'),
    'comments',
    'comments'
);
// Now you need to fullfill existing rows with data from address table


$this->endSetup();