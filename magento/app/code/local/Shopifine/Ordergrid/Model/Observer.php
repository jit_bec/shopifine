<?php

class Shopifine_Ordergrid_Model_Observer {
    public function addColumnToResource(Varien_Event_Observer $observer){
        /* @var $resource Mage_Sales_Model_Mysql4_Order */
        
        //Mage::dispatchEvent('sales_model_service_quote_submit_after');
       $resource = $observer->getEvent()->getResource();
//        $resource->addVirtualGridColumn(
//            'delivery_slot',
//            'shopifine_checkout/custom_order',
//            array('entity_id' => 'order_id'),
//            'delivery_slot'
//        );
//        $resource->addVirtualGridColumn(
//            'delivery_date',
//            'shopifine_checkout/custom_order',
//            array('entity_id' => 'order_id'),
//            'delivery_date'
//        );
//        $resource->addVirtualGridColumn(
//            'comments',
//            'shopifine_checkout/custom_order',
//            array('entity_id' => 'order_id'),
//            'comments'
//        );
        $resource->addVirtualGridColumn(
            'postcode',
            'sales/order_address',
            array('shipping_address_id' => 'entity_id'),
            'postcode'
        );
        $resource->addVirtualGridColumn(
            'street',
            'sales/order_address',
            array('shipping_address_id' => 'entity_id'),
            'street'
        );
    }
}