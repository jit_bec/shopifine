<?php

abstract class Shopifine_Ordergrid_Model_Api2_Invoice_Item_Rest 
     extends Shopifine_Ordergrid_Model_Api2_Invoice_Item 

{
    const PARAM_ORDER_ID = 'id';
    /**#@-*/

    /**
     * Get order items list
     *
     * @return array
     */
    protected function _addOrderId(Mage_Sales_Model_Resource_Order_Invoice_Item_Collection $collection)
    {
        $collection->getSelect()->join(
            array('invoice_flat' => $collection->getTable('sales/invoice')),
            'main_table.parent_id = invoice_flat.entity_id',
            array(
                'order_id' => 'invoice_flat.order_id'
            )
        );

        return $this;
    }
    
    
    protected function _retrieveCollection()
    {
        $data = array();
        /* @var $item Mage_Sales_Model_Order_Item */
        foreach ($this->_getCollectionForRetrieve() as $item) {
            
            $itemData = $item->getData();
            $itemData['status'] = $item->getStatus();
            $data[] = $itemData;
        }
        return $data;
    }
    /**
     * Retrieve order items collection
     *
     * @return Mage_Sales_Model_Resource_Order_Item_Collection
     */
    protected function _getCollectionForRetrieve()
    {
        /* @var $order Mage_Sales_Model_Order */
        $invoice = $this->_loadInvoiceById(
            $this->getRequest()->getParam(self::PARAM_ORDER_ID)
        );

        /* @var $collection Mage_Sales_Model_Resource_Order_Item_Collection */
        $invoiceId = $invoice->getId();
        $collection = Mage::getResourceModel('sales/order_invoice_item_collection');
        $collection->addFieldToFilter('parent_id', $invoiceId);
        $this->_addOrderId($collection);
        $this->_applyCollectionModifiers($collection);
        return $collection;
    }

    /**
     * Load order by id
     *
     * @param int $id
     * @throws Mage_Api2_Exception
     * @return Mage_Sales_Model_Order
     */
    protected function _loadInvoiceById($id)
    {
        /* @var $invoice Mage_Sales_Model_Order_Invoice */
        //$order = Mage::getModel('sales/order_invoice')->load($id);
        $collection = Mage::getResourceModel('sales/order_invoice_collection')->addFieldToFilter('increment_id', $id);;
        $invoice = $collection->getItemByColumnValue('increment_id',$id);
        if (!$invoice->getId()) {
            $this->_critical(self::RESOURCE_NOT_FOUND);
        }
        return $invoice;
    }
}