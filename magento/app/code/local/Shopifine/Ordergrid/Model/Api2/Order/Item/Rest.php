<?php

class Shopifine_Ordergrid_Model_Api2_Order_Item_Rest extends Mage_Sales_Model_Api2_Order_Item_Rest {
    
    protected function _loadOrderById($id)
    {
        /* @var $order Mage_Sales_Model_Order */
        //$order = Mage::getModel('sales/order')->load($id);
         $collection = Mage::getResourceModel('sales/order_collection')->addFieldToFilter('increment_id', $id);;
        $order = $collection->getItemByColumnValue('increment_id',$id);
        if (!$order->getId()) {
            $this->_critical(self::RESOURCE_NOT_FOUND);
        }
        return $order;
    }
}