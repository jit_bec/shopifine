<?php

class Shopifine_Ordergrid_Model_Mysql4_Report_Collection extends Mage_Reports_Model_Resource_Report_Collection{
    public function getReportFull($from, $to)
    {
        $model = $this->_model;
        $fromDate = Mage::app()->getLocale()
            ->utcDate(null, $from, true, Varien_Date::DATETIME_INTERNAL_FORMAT);
        $toDate = Mage::app()->getLocale()
            ->utcDate(null, $to, true, Varien_Date::DATETIME_INTERNAL_FORMAT);
        $fromDate->subHour(6);
        $toDate->subHour(6);
        $from = $fromDate->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
        $to = $toDate->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
        return $this->_model->getReportFull($from, $to);
    }
    
    public function getReport($from, $to)
    {
         $model = $this->_model;
        $fromDate = Mage::app()->getLocale()
            ->utcDate(null, $from, true, Varien_Date::DATETIME_INTERNAL_FORMAT);
        $toDate = Mage::app()->getLocale()
            ->utcDate(null, $to, true, Varien_Date::DATETIME_INTERNAL_FORMAT);
        $fromDate->subHour(6);
        $toDate->subHour(6);
        $from = $fromDate->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
        $to = $toDate->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
        return $this->_model->getReport($from, $to);
    }
}