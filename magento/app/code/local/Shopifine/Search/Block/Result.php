<?php

class Shopifine_Search_Block_Result extends Mage_CatalogSearch_Block_Result {
    
    public function getResultCount()
    {
        if (!$this->getData('result_count')) {
            $size = $this->_getProductCollection()->getSize();
            $this->_getQuery()->setNumResults($size);
            $this->setResultCount($size);
        }
        
        $count = $this->getData('result_count');
        $query = $this->helper('catalogsearch')->getQuery();
        $catSession = Mage::getSingleton('catalogsearch/session');
        $catSession->setData('result_count', $count);
        $catSession->setData('queryId', $query->getId());
        $catSession->setData('queryText', $query->getQueryText());
        return $count;
        
    }
    
}
?>
