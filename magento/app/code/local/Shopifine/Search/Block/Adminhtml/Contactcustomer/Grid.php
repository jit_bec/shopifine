<?php

class Shopifine_Search_Block_Adminhtml_Contactcustomer_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('contactcustomerGrid');
        $this->setDefaultSort('query_text');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('shopifine_search/custom_search')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
 
    protected function _prepareColumns()
    {
        
 
        $this->addColumn('query_text', array(
          'header'    => Mage::helper('shopifine_search')->__('Item Searched'),
          'align'     =>'left',
          'index'     => 'query_text',
          'width'     => '50px',
        ));
        $this->addColumn('email', array(
          'header'    => Mage::helper('shopifine_search')->__('Email'),
          'align'     =>'left',
          'index'     => 'email',
          'width'     => '50px',
        ));
        $this->addColumn('mobile_number', array(
          'header'    => Mage::helper('shopifine_search')->__('Mobile'),
          'align'     =>'left',
          'index'     => 'mobile_number',
          'width'     => '50px',
          
        ));
        $this->addColumn('landline_number', array(
          'header'    => Mage::helper('shopifine_search')->__('Landline'),
          'align'     =>'left',
          'index'     => 'landline_number',
          'width'     => '50px',
        ));
        $this->addColumn('comments', array(
          'header'    => Mage::helper('shopifine_search')->__('Comments'),
          'align'     =>'left',
          'index'     => 'comments',
          'width'     => '50px',
        ));
        $this->addColumn('quantity', array(
          'header'    => Mage::helper('shopifine_search')->__('Quantity'),
          'align'     =>'left',
          'index'     => 'quantity',
          'width'     => '50px',
        ));
 
          
        
        return parent::_prepareColumns();
    }
}
?>
