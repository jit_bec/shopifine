<?php

class Shopifine_Search_Block_Adminhtml_Contactcustomer extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_contactcustomer';
    $this->_blockGroup = 'shopifine_search';
   
    $this->_headerText = Mage::helper('shopifine_search')->__('Contact Customer For Item Not Found');
    //$this->_addButtonLabel = Mage::helper('shopifine_search')->__('Add Employee');
    parent::__construct();
     $this->removeButton('add');
  }
}
?>