<?php

$installer = $this;

$installer->startSetup();

$installer->run("

CREATE TABLE IF NOT EXISTS {$this->getTable('catalogsearch_shopifine')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `query_id` int(11) unsigned,
  `query_text` varchar(255) ,
  `email` varchar(50) ,
  `mobile_number` varchar(50) ,
  `landline_number` varchar(50),
  `comments` text ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 