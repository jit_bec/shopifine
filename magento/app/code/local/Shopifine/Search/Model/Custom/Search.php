<?php

class Shopifine_Search_Model_Custom_Search extends Mage_Core_Model_Abstract {
    public function _construct() {
        parent::_construct();
        $this->_init('shopifine_search/custom_search');
    }
    
}