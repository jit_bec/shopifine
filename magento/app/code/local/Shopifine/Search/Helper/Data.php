<?php

class Shopifine_Search_Helper_Data extends Mage_Core_Helper_Abstract{
    
    const QUERY_SESSION_NAME = 'queryText';
    const QUERY_RESULT_COUNT = 'result_count';
    
    public function getContactUrl()
    {
        return $this->_getUrl('shopifinesearch/search/contact');
    }
    
    public function storeQueryTextInSession()
    {
        $queryText = $this->_getRequest()->getParam($this->getQueryParamName());
        $catSession = Mage::getSingleton('catalogsearch/session');
        $catSession->setData(self::QUERY_SESSION_NAME, $queryText);
        
    }
    
    public function getQueryTextFromSession()
    {
        
        $catSession = Mage::getSingleton('catalogsearch/session');
        return $catSession->getData(self::QUERY_SESSION_NAME);
    }
    
    public function resetQueryTextInSession()
    {
        
        $catSession = Mage::getSingleton('catalogsearch/session');
        $catSession->unsetData(self::QUERY_SESSION_NAME);
    }
    
    public function isQueryTextInSession()
    {
        
        $catSession = Mage::getSingleton('catalogsearch/session');
        return $catSession->hasData(self::QUERY_SESSION_NAME);
        
    }
    
     
}
?>
