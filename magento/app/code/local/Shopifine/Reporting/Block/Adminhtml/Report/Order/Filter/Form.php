<?php

class Shopifine_Reporting_Block_Adminhtml_Report_Order_Filter_Form extends Mage_Adminhtml_Block_Report_Filter_Form
{
    /**
     * Add fields to base fieldset which are general to sales reports
     *
     * @return Mage_Sales_Block_Adminhtml_Report_Filter_Form
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $form = $this->getForm();
        $htmlIdPrefix = $form->getHtmlIdPrefix();
        /** @var Varien_Data_Form_Element_Fieldset $fieldset */
        $fieldset = $this->getForm()->getElement('base_fieldset');

        if (is_object($fieldset) && $fieldset instanceof Varien_Data_Form_Element_Fieldset) {

            

            $fieldset->addField('time_from', 'select', array(
                'name'      => 'time_from',
                'label'     => Mage::helper('reporting')->__('Time From'),
                'options'   => array(
                        '0' => Mage::helper('reporting')->__('Any'),
                        '1' => Mage::helper('reporting')->__('Specified'),
                    ),
                'note'      => Mage::helper('reporting')->__('Applies to Any of the Specified Order Statuses'),
            ), 'to');
            $fieldset->addField('time_to', 'select', array(
                'name'      => 'time_to',
                'label'     => Mage::helper('reporting')->__('Time To'),
                'options'   => array(
                        '0' => Mage::helper('reporting')->__('Any'),
                        '1' => Mage::helper('reporting')->__('Specified'),
                    ),
                'note'      => Mage::helper('reporting')->__('Applies to Any of the Specified Order Statuses'),
            ), 'to');

           
            // define field dependencies
//            if ($this->getFieldVisibility('time_from') && $this->getFieldVisibility('time_to')) {
//                $this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
//                    ->addFieldMap("{$htmlIdPrefix}time_from", 'time_from')
//                    ->addFieldMap("{$htmlIdPrefix}time_to", 'time_to')
//                    ->addFieldDependence('time_to', 'time_from', '1')
//                );
//            }
        }

        return $this;
    }
}
