<?php

class Shopifine_Reporting_Block_Adminhtml_Report_Order_Grid extends Mage_Adminhtml_Block_Report_Grid_Abstract
{
 
    protected $_columnGroupBy = 'period';
 
    public function __construct()
    {
        parent::__construct();
        $this->setCountTotals(true);
    }
 
    public function getResourceCollectionName()
    {
        return 'reporting/report_order_collection';
    }
 
    protected function _prepareColumns()
    {
        $this->addColumn('period', array(
            'header'        => Mage::helper('adminhtml')->__('Period'),
            'index'         => 'period',
            'width'         => 100,
            'sortable'      => false,
            'period_type'   => $this->getPeriodType(),
            'renderer'      => 'adminhtml/report_sales_grid_column_renderer_date',
            'totals_label'  => Mage::helper('adminhtml')->__('Total'),
            'html_decorators' => array('nobr'),
        ));

        $this->addColumn('product_name', array(
            'header'    => Mage::helper('adminhtml')->__('Product Name'),
            'index'     => 'product_name',
            'type'      => 'string',
            'sortable'  => false
        ));

        $baseCurrencyCode = $this->getCurrentCurrencyCode();

        $this->addColumn('price', array(
            'header'        => Mage::helper('reporting')->__('Price'),
            'width'         => '120px',
            'type'          => 'currency',
            'currency_code' => $baseCurrencyCode,
            'index'         => 'price',
            'rate'          => $this->getRate($baseCurrencyCode),
        ));

        $this->addColumn('ordered_qty', array(
            'header'    =>Mage::helper('reporting')->__('Quantity Ordered'),
            'width'     =>'120px',
            'align'     =>'right',
            'index'     =>'ordered_qty',
            'total'     =>'sum',
            'type'      =>'number'
        ));

        $this->addExportType('*/*/exportOrderedByTimeCsv', Mage::helper('reporting')->__('CSV'));
        $this->addExportType('*/*/exportOrderedByTimeExcel', Mage::helper('reporting')->__('Excel XML'));

        return parent::_prepareColumns();
    }
}