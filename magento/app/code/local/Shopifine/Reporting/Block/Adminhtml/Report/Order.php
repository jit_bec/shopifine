<?php

class Shopifine_Reporting_Block_Adminhtml_Report_Order extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'reporting';
        $this->_controller = 'adminhtml_report_order';
        $this->_headerText = Mage::helper('reporting')->__('Orders by time');
        
        parent::__construct();
        $this->setTemplate('report/grid/container.phtml');
        $this->_removeButton('add');
        $this->addButton('filter_form_submit', array(
            'label'     => Mage::helper('reporting')->__('Show Report'),
            'onclick'   => 'filterFormSubmit()'
        ));
    }
    public function getFilterUrl()
    {
        $this->getRequest()->setParam('filter', null);
        return $this->getUrl('*/*/orderedByTime', array('_current' => true));
    }
    
//    public function __construct()
//    {
//        $this->_controller = 'report_sales_bestsellers';
//        $this->_headerText = Mage::helper('sales')->__('Products Bestsellers Report');
//        parent::__construct();
//        $this->setTemplate('report/grid/container.phtml');
//        $this->_removeButton('add');
//        $this->addButton('filter_form_submit', array(
//            'label'     => Mage::helper('reports')->__('Show Report'),
//            'onclick'   => 'filterFormSubmit()'
//        ));
//    }
//
//    public function getFilterUrl()
//    {
//        $this->getRequest()->setParam('filter', null);
//        return $this->getUrl('*/*/bestsellers', array('_current' => true));
//    }
 
}