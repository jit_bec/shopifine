<?php

class Shopifine_Reporting_Model_Mysql4_Report_Order extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('reporting/order', 'entity_id');
    }

    public function init($table, $field = 'id') {
        $this->_init($table, $field);
        return $this;
    }

}