<?php

class Shopifine_Reporting_Model_Report_Order extends Mage_Core_Model_Abstract {
    
    public function _construct() {
        parent::_construct();
        $this->_init('reporting/order');
    }
}