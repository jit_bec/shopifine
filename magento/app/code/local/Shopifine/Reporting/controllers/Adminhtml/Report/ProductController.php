<?php

class Shopifine_Reporting_Adminhtml_Report_ProductController extends Mage_Adminhtml_Controller_Action {

    public function _initAction() {
        $this->loadLayout()
                ->_addBreadcrumb(Mage::helper('reporting')->__('Reports'), Mage::helper('reporting')->__('Reports'));
        return $this;
    }

    public function _initReportAction($blocks) {
        if (!is_array($blocks)) {
            $blocks = array($blocks);
        }

        $requestData = Mage::helper('adminhtml')->prepareFilterString($this->getRequest()->getParam('filter'));
        $requestData = $this->_filterDates($requestData, array('from', 'to'));
        $params = new Varien_Object();

        foreach ($requestData as $key => $value) {
            if (!empty($value)) {
                $params->setData($key, $value);
            }
        }

        foreach ($blocks as $block) {
            if ($block) {
                $block->setPeriodType($params->getData('period_type'));
                $block->setFilterData($params);
            }
        }
        return $this;
    }

    public function orderedByTimeAction() {
        $this->_title($this->__('Reports'))->_title($this->__('Shopifine Custom Reports'))->_title($this->__('Orders by time'));

        $this->_initAction()
                ->_setActiveMenu('report/reporting/orders')
                ->_addBreadcrumb(Mage::helper('reporting')->__('Orders by time'), Mage::helper('reporting')->__('Orders by time'));

        $gridBlock = $this->getLayout()->getBlock('adminhtml_report_order.grid');
       
        $filterFormBlock = $this->getLayout()->getBlock('grid.filter.form');
       
        $this->_initReportAction(array(
        $gridBlock,
        $filterFormBlock
        ));
       
        $this->renderLayout();

       
    }

    public function exportOrderedByTimeCsvAction()
    {
        $fileName   = 'shopifineorder.csv';
        $grid       = $this->getLayout()->createBlock('reporting/adminhtml_report_order_grid');
        $this->_initReportAction($grid);
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile($fileName));
    }
   
    public function exportOrderedByTimeExcelAction()
    {
        $fileName   = 'shopifineorder.xml';
        $grid       = $this->getLayout()->createBlock('reporting/adminhtml_report_order_grid');
        $this->_initReportAction($grid);
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

}