<?php

class Shopifine_Setup_Block_Html_Breadcrumbs extends Mage_Page_Block_Html_Breadcrumbs{
    
    function addCrumb($crumbName, $crumbInfo, $after = false)
    {
     
//        $label = $crumbInfo['label'];
        $alterLink = Mage::getStoreConfig('shopifine/url/breadcrumbhome');  
        //$link = $crumbInfo['link']; //do we need to validate this as well??
        if ($crumbName == 'home' && !empty($alterLink)){ //if alter link is set in system config redefine the home crumb
            $crumbInfo['link'] = $alterLink;
        }
//        $this->_prepareArray($crumbInfo, array('label', 'title', 'link', 'first', 'last', 'readonly'));
//        if ((!isset($this->_crumbs[$crumbName])) || (!$this->_crumbs[$crumbName]['readonly'])) {
//           $this->_crumbs[$crumbName] = $crumbInfo;
//        }
//        return $this;
        return parent::addCrumb($crumbName, $crumbInfo,$after);
    }
}
?>
