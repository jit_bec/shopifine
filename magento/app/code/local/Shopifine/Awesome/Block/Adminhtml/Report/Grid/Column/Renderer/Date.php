<?php

class Shopifine_Awesome_Block_Adminhtml_Report_Grid_Column_Renderer_Date
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Date
{
    /**
     * Retrieve date format
     *
     * @return string
     */
    protected function _getFormat()
    {
        $format = 'dd-MM-yyyy HH:mm:ss';
        return $format;
    }

    /**
     * Renders grid column
     *
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        if ($data = $row->getData($this->getColumn()->getIndex())) {
            
            $format = $this->_getFormat();
            $period = $this->getColumn()->getPeriodType();
            try {
                $data = 
                    Mage::app()->getLocale()->date($data, $dateFormat)->toString($format);
                    
            }
            catch (Exception $e) {
                $data = ($this->getColumn()->getTimezone())
                    ? Mage::app()->getLocale()->date($data, $dateFormat)->toString($format)
                    : Mage::getSingleton('core/locale')->date($data, $dateFormat, null, false)->toString($format);
            }
            return $data;
        }
        return $this->getColumn()->getDefault();
    }
}