<?php
 
class Shopifine_Awesome_Block_Adminhtml_Report_Ordered_Grid extends Mage_Adminhtml_Block_Report_Grid
{
 
   //protected $_filters = array('test' => '');
      protected $_subReportSize = 0;
   
    public function __construct()
    {
        parent::__construct();
        $this->setId('gridShopifineProducts');
    }

    protected function _prepareCollection()
    {
        parent::_prepareCollection();
        $this->getCollection()->initReport('awesome/product_ordered_collection');
    }

    protected function _prepareColumns()
    {
        $this->addColumn('name', array(
            'header'    =>Mage::helper('reports')->__('Product Name'),
            'index'     =>'name'
        ));

//        $baseCurrencyCode = $this->getCurrentCurrencyCode();
//
//        $this->addColumn('price', array(
//            'header'        => Mage::helper('reports')->__('Price'),
//            'width'         => '120px',
//            'type'          => 'currency',
//            'currency_code' => $baseCurrencyCode,
//            'index'         => 'price',
//            'rate'          => $this->getRate($baseCurrencyCode),
//        ));

        $this->addColumn('ordered_qty', array(
            'header'    =>Mage::helper('reports')->__('Quantity Ordered'),
            'width'     =>'120px',
            'align'     =>'right',
            'index'     =>'ordered_qty',
            'total'     =>'sum',
            'type'      =>'number'
        ));
        
        $this->addColumn('created_at', array(
            'header'    =>Mage::helper('reports')->__('Created AT'),
            'width'     =>'120px',
            'align'     =>'right',
            'index'     =>'created_at',
            'renderer'  =>'awesome/adminhtml_report_grid_column_renderer_date'
            
        ));
        
        

        $this->addExportType('*/*/exportOrderedCsv', Mage::helper('reports')->__('CSV'));
        $this->addExportType('*/*/exportOrderedExcel', Mage::helper('reports')->__('Excel XML'));
        

        return parent::_prepareColumns();
    }
}