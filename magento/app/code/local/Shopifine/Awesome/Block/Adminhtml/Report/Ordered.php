<?php

class Shopifine_Awesome_Block_Adminhtml_Report_Ordered extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'awesome';
        $this->_controller = 'adminhtml_report_ordered';
        $this->_headerText = Mage::helper('awesome')->__('Custom Report'); 
        parent::__construct();
        $this->_removeButton('add');
        
    }
//    public function getFilterUrl()
//    {
//        $this->getRequest()->setParam('filter', null);
//        return $this->getUrl('*/*/ordered', array('_current' => true));
//    }
}
