<?php

class Shopifine_Awesome_Model_Mysql4_Product_Ordered_Collection extends Mage_Reports_Model_Resource_Product_Ordered_Collection {

    protected function _joinFields($from = '', $to = '') {
        $this->addAttributeToSelect('*')
                ->addOrderedQty($from, $to)
                ->setOrder('ordered_qty', self::SORT_ORDER_DESC);

        return $this;
    }

    public function addOrderedQty($from = '', $to = '') {
        $adapter = $this->getConnection();
        $compositeTypeIds = Mage::getSingleton('catalog/product_type')->getCompositeTypes();
        $orderTableAliasName = $adapter->quoteIdentifier('order');
        $productDetailsTableAliasName = $adapter->quoteIdentifier('details');

        $orderJoinCondition = array(
            $orderTableAliasName . '.entity_id = order_items.order_id',
            $adapter->quoteInto("{$orderTableAliasName}.state <> ?", Mage_Sales_Model_Order::STATE_CANCELED),
        );
            
        $detailsJoinCondition = array(
            $productDetailsTableAliasName . '.entity_id = order_items.product_id',
            $productDetailsTableAliasName.'.attribute_id =96'
        );
            
            //`catalog_product_entity_varchar` AS`details` ON   details.entity_id= order_items.product_id  AND details.attribute_id=96

       $productJoinCondition = array(
            'e.entity_id = order_items.product_id',
            $adapter->quoteInto('e.entity_type_id = ?', $this->getProductEntityTypeId())
        );

        if ($from != '' && $to != '') {
            $fieldName = $orderTableAliasName . '.created_at';
            $orderJoinCondition[] = $this->_prepareBetweenSql($fieldName, $from, $to);
        }

        $this->getSelect()->reset()
                ->from(
                        array('order_items' => $this->getTable('sales/order_item')), array(
                    'ordered_qty' => 'SUM(order_items.qty_ordered)'
                ))
                ->joinInner(
                        array('order' => $this->getTable('sales/order')), implode(' AND ', $orderJoinCondition), array())
                ->joinLeft(
                        array('e' => $this->getProductEntityTableName()), implode(' AND ', $productJoinCondition), array(
                    'entity_id' => 'order_items.product_id',
                    'entity_type_id' => 'e.entity_type_id',
                    'attribute_set_id' => 'e.attribute_set_id',
                    'type_id' => 'e.type_id',
                    'sku' => 'e.sku',
                    'has_options' => 'e.has_options',
                    'required_options' => 'e.required_options',
                    'created_at' => 'order.created_at',
                    'updated_at' => 'e.updated_at'
                ))
                ->joinLeft(
                        array('details' => $this->getTable('catalog_product_entity_varchar')), implode(' AND ', $detailsJoinCondition), array(
                    'order_items_name' => 'details.value'
                ))
                //->where(array('product_type=?','simple'))
                ->where ( $adapter->quoteInto('product_type=?','simple').' OR '.$adapter->quoteInto('product_type=?','grouped'))
                ->group('order_items.product_id')
                ->having('SUM(order_items.qty_ordered) > ?', 0);
        //var_dump($this->getSelect()->__toString());
        $selectString = $this->getSelect()->__toString();
        //Abhijit;
        //echo $this->getSelect();
        return $this;
    }

}