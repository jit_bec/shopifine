<?php

class Shopifine_Awesome_Model_Ordered extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('awesome/ordered');
    }
}