<?php

class Shopifine_Awesome_Adminhtml_Report_ProductController extends Mage_Adminhtml_Controller_Action {
    
    public function _initAction()
    {
 
        $this->loadLayout()
        ->_addBreadcrumb(Mage::helper('awesome')->__('Awesome'), Mage::helper('awesome')->__('Awesome'));
        return $this;
    }
 
    
 
    public function orderedAction()
    {
        $this->_title($this->__('Awesome'))->_title($this->__('Reports'))->_title($this->__('Complex Report'));
 
        
 
        $this->_initAction()
        ->_setActiveMenu('reports/shopifine/order')
        ->_addBreadcrumb(Mage::helper('awesome')->__('Simple Example Report'), Mage::helper('awesome')->__('Simple Example Report'))
        ->_addContent($this->getLayout()->createBlock('awesome/adminhtml_report_ordered'))
        ->renderLayout();
 
        
 
    }
 
    public function exportOrderedCsvAction()
    {
        $fileName   = 'simple.csv';
        $content    = $this->getLayout()->createBlock('awesome/adminhtml_report_ordered_grid')
        ->getCsv();
 
        $this->_prepareDownloadResponse($fileName, $content);
    }
 

}